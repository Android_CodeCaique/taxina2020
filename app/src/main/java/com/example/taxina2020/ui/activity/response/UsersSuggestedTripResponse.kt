package com.example.taxina2020.ui.activity.response

data class UsersSuggestedTripResponse(
    val avaliable_num: Int,
    val block: Int,
    val data: List<UsersSuggestedTripData>,
    val error: Int,
    val message: String,
    val ride_num: Int,
    val type: String
)

data class UsersSuggestedTripData(
    val first_name: String,
    val gender: String,
    val id: Int,
    val image: String,
    val is_ride: String,
    val last_name: String,
    val money: String,
    val phone: String,
    val seat_num: String,
    val user_id: String
)