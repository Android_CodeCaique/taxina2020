package com.example.taxina2020.ui.activity.all.online.nationalityTrip

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.databinding.FragmentNationalityTripsBinding
import com.example.taxina2020.ui.activity.all.online.createRide.CreateRideViewModel


class NationalityTrips : Fragment() {

    lateinit var binding : FragmentNationalityTripsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentNationalityTripsBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val model = ViewModelProviders.of(this).get(NationalityTripViewModel::class.java)
        binding.vm = model


        return binding.root
    }


}