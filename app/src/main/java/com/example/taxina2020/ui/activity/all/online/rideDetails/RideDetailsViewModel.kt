package com.example.taxina2020.ui.activity.all.online.rideDetails

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.ui.activity.response.UsersSuggestedTripData
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class RideDetailsViewModel : BaseViewModel() {

    var locationFrom = MutableLiveData<String>("")
    var locationTo = MutableLiveData<String>("")
    var date = MutableLiveData<String>("")
    var tripId = MutableLiveData<Int>(0)
    var available = MutableLiveData<String>("")
    var ride = MutableLiveData<String>("")

    var loader = MutableLiveData<Boolean>(false)
    var sharedHelper: SharedHelper = SharedHelper()
    var users: MutableLiveData<List<UsersSuggestedTripData>> = MutableLiveData()

    var btnText = MutableLiveData<String>("")

    fun getTripUsers(context: Context)
    {
        loader.value = true
        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {
            val response = service.showUsersSpecialTrip("Bearer ${sharedHelper.getKey(context, "Token")}",tripId.value!!)
            withContext(Dispatchers.Main) {
                try {
                    Log.i("response1",response.message())
                    if (response.isSuccessful && response.body()!!.error == 0) {
                        loader.value = false
                        users.value = response.body()!!.data
                        available.value = response.body()!!.avaliable_num.toString()
                        ride.value = response.body()!!.ride_num.toString()
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        Log.i("response2",response.message())
                        when (response.body()!!.type) {
                            "wait" -> btnText.value = "Arrive"
                            "start" -> btnText.value = "End trip"
                            "end" -> btnText.value = "Trip Ended"
                            else ->{
                                btnText.value = "Unknown Case"
                            }
                        }

                    } else {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                        Log.i("response3",response.message())
                        users.value = response.body()!!.data
                        available.value = response.body()!!.avaliable_num.toString()
                        ride.value = response.body()!!.ride_num.toString()

                    }
                }catch (e:Exception)
                {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false
                    Log.i("response4",response.message())

                }
            }
        }
    }

    fun startSpechialTrip(v: View)
    {
        if(btnText.value == "Arrive") {
            loader.value = true
            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                val response = service.startSpecialTrip("Bearer ${sharedHelper.getKey(v.rootView.context, "Token")}", tripId.value!!)
                withContext(Dispatchers.Main) {
                    try {
                        Log.i("response1", response.message())
                        if (response.isSuccessful && response.body()!!.error == 0) {
                            loader.value = false

                            Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                            Log.i("response2", response.message())
                            when (response.body()!!.message) {
                                "special trip is startes" -> {
                                    btnText.value = "End trip"
                                    sharedHelper.putKey(v.rootView.context , "exitPage" , "false")}
                                "special trip is ended" -> {
                                    btnText.value = "Arrive"
                                    sharedHelper.putKey(v.rootView.context , "exitPage" , "true")
                                }
                                else -> {
                                }
                            }

                        } else {
                            Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                            loader.value = false
                            Log.i("response3", response.message())

                        }
                    } catch (e: Exception) {
                        Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                        loader.value = false
                        Log.i("response4", response.message())

                    }
                }
            }
        }else if(btnText.value == "End trip")
        {
            loader.value = true
            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                val response = service.endSpecialTrip("Bearer ${sharedHelper.getKey(v.rootView.context, "Token")}", tripId.value!!)
                withContext(Dispatchers.Main) {
                    try {
                        Log.i("response1", response.message())
                        if (response.isSuccessful && response.body()!!.error == 0) {
                            loader.value = false

                            Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                            Log.i("response2", response.message())
                            when (response.body()!!.message) {
                                "special trip is startes" -> btnText.value = "End trip"
                                "special trip is ended" -> btnText.value = "Arrive"
                                else -> {
                                }
                            }

                        } else {
                            Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                            loader.value = false
                            Log.i("response3", response.message())

                        }
                    } catch (e: Exception) {
                        Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                        loader.value = false
                        Log.i("response4", response.message())

                    }
                }
            }
        }
    }

    fun showAlert(v: View )
    {
        val alert: Dialog? = Dialog(v.rootView.context)
        alert?.setContentView(R.layout.start_trip_alert)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.confirmBtnId)?.setOnClickListener {
            Log.i("SharedHelperAlert",sharedHelper.getKey(v.rootView.context, "GoToLogin").toString())
            startSpechialTrip(v)
            alert.dismiss()

        }
        alert?.findViewById<Button>(R.id.cancelBtnId)?.setOnClickListener {
            alert.dismiss()
        }

        alert?.show()
    }

    fun showAllTripSeats(v: View)
    {
        val action = RideDetailsDirections.actionRideDetailsToOnlineAsTaxi1212(tripId.value!!)
        Navigation.findNavController(v).navigate(action)
    }
}


//special trip is startes
//special trip is ended