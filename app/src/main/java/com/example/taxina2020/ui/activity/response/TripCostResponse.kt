package com.example.taxina2020.ui.activity.response

data class TripCostResponse(
    val error: Int,
    val message: String,
    val total_price: Int,
    val your_gain: Double
)