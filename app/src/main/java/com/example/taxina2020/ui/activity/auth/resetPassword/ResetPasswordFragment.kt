package com.example.taxina2020.ui.activity.auth.resetPassword

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentResetPasswordBinding

class ResetPasswordFragment : Fragment() {

    lateinit var binding : FragmentResetPasswordBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = FragmentResetPasswordBinding.inflate(inflater)
        binding.lifecycleOwner  = this

        val vm = ViewModelProviders.of(this).get(ResetPasswordViewModel::class.java)
        binding.vm = vm


        return binding.root
    }

}