
import com.example.taxina2020.ui.activity.networking.Auth
import com.example.taxina2020.ui.activity.networking.CountryUser
import com.example.taxina2020.ui.activity.networking.Home
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


object ApiClient {
//   var BASE_URL:String="https://gist.githubusercontent.com/"
//    val getClient: ApiInterface
//        get() {
//
//            val gson = GsonBuilder()
//                .setLenient()
//                .create()
//            val interceptor = HttpLoggingInterceptor()
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
//            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
//
//            val retrofit = Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build()
//
//
//            return retrofit.create(ApiInterface::class.java)
//
//        }

    const val BASE_URL = "http://taxiApi.codecaique.com/api/"

        fun makeRetrofitService(): Auth {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create().asLenient())
            .build().create(Auth::class.java)
        }

    fun makeRetrofitServiceHome(): Home {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .build().create(Home::class.java)
    }

    fun makeGoogleDirectionsAPISericve() : Home{
        return Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/directions/")
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .build().create(Home::class.java)
    }

    fun makeUserAPIService() : CountryUser{
        return Retrofit.Builder()
                .baseUrl("http://taxiApi.codecaique.com/api/")
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .build().create(CountryUser::class.java)
    }



//
//        fun makeRetrofitServiceHome(): Home {
//
////        val client : OkHttpClient = OkHttpClient()
////        client.connectTimeoutMillis().and(1000)
////        client.readTimeoutMillis().and(1000)
////        client.setConnectTimeout(10, TimeUnit.SECONDS)
////        client.setReadTimeout(30, TimeUnit.SECONDS)
////        client.interceptors().add(Interceptor { chain -> onOnIntercept(chain) })
////
//
////        val httpClient = OkHttpClient
////            .connectTimeout(5, TimeUnit.MINUTES)
////            .readTimeout(5, TimeUnit.MINUTES)
////            .addInterceptor {
////                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
////            }
//
//            val okHttpClient = OkHttpClient().newBuilder()
//                .connectTimeout(20, TimeUnit.SECONDS)
//                .build()
//
//            return Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(MoshiConverterFactory.create().asLenient())
//                .client(okHttpClient)
//                .build()
//                .create(Home::class.java) // create module after using Home class
//        }






//        fun makeRetrofitServiceGeneral(): General{
//            return Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(MoshiConverterFactory.create().asLenient())
//                .build().create(General::class.java)
//        }




//    val netModule = module {
//        fun provideCache(application: Application): Cache {
//            val cacheSize = 10 * 1024 * 1024
//            return Cache(application.cacheDir, cacheSize.toLong())
//        }
//
//        fun provideHttpClient(cache: Cache): OkHttpClient {
//            val okHttpClientBuilder = OkHttpClient.Builder()
//                .cache(cache)
//
//            return okHttpClientBuilder.build()
//        }
//
//        fun provideGson(): Gson {
//            return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create()
//        }


//        fun provideRetrofit(factory: Gson, client: OkHttpClient): Retrofit {
//            return Retrofit.Builder()
//                .baseUrl("https://newsapi.org/")
//                .addConverterFactory(GsonConverterFactory.create(factory))
//                .addCallAdapterFactory(CoroutineCallAdapterFactory())
//                .client(client)
//                .build()
//        }

//        single { provideCache(androidApplication()) }
//        single { provideHttpClient(get()) }
//        single { provideGson() }
//        single { provideRetrofit(get(), get()) }
//    }

}