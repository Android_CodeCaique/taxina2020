package com.example.taxina2020.ui.activity.response

data class UserSelectedSeatResponse(
    val data: List<UserSelectedSeatData>,
    val error: Int,
    val message: String
)

data class UserSelectedSeatData(
    val id: Int,
    val is_ride: String,
    val money: String,
    val seat_num: String,
    val user_id: String
)