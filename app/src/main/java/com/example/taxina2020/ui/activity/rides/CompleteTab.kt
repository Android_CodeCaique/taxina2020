package com.example.taxina2020.ui.activity.rides

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.databinding.FragmentCompleteTabBinding
import com.example.taxina2020.ui.activity.response.CompletedTripData
import com.example.taxina2020.ui.activity.response.UpcomingBusData
import com.example.taxina2020.ui.activity.utils.SharedHelper

/**
 * A simple [Fragment] subclass.
 */
class CompleteTab : Fragment() {

    lateinit var binding: FragmentCompleteTabBinding
    var trips: MutableLiveData<List<CompletedTripData>> = MutableLiveData()
    private var sharedHelper: SharedHelper? = null
    var usersBusCompleted: MutableLiveData<List<UpcomingBusData>> = MutableLiveData()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentCompleteTabBinding.inflate(inflater)
        binding.lifecycleOwner = this

        sharedHelper = SharedHelper()
        val ridesViewModel = ViewModelProviders.of(this).get(RidesViewModel::class.java)
        binding.vm = ridesViewModel

        if (sharedHelper?.getKey(requireActivity(), "car_type") == "1") {
            ridesViewModel.getCompletedTrips(requireContext())
            ridesViewModel.trips.observe(viewLifecycleOwner, Observer {
                if (it.isNotEmpty()) {
                    trips.value = it
                    binding.tripsRecyclerId.adapter = CompletedTripRecyclerAdapter(trips, activity)
                }
            })
        }

        if (sharedHelper?.getKey(requireActivity(), "car_type") == "2") {
            Log.i("Rides","2")

            ridesViewModel.getBusCompletedTrips(requireContext())
            ridesViewModel.usersBusCompleted.observe(viewLifecycleOwner, Observer {
                usersBusCompleted.value = it
                binding.tripsRecyclerId.adapter =  SpecialTripBusRecyclerAdapter(usersBusCompleted,requireContext())

            })

        }

        return binding.root
    }
}