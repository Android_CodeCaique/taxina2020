package com.example.taxina2020.ui.activity.response

data class ForgetPasswordResponse(
    val code: String,
    val error: Int,
    val message: String
)