package com.example.taxina2020.ui.activity.rides

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.taxina2020.R
import com.example.taxina2020.databinding.LayoutProcessOneBinding
import com.example.taxina2020.databinding.RidersCompletedRowBinding
import com.example.taxina2020.databinding.RidersSpecialTripRowBinding
import com.example.taxina2020.ui.activity.response.CompletedTripData
import kotlinx.android.synthetic.main.dialog_cancel.*

class SpecialTripRecyclerAdapter (private var dataList: LiveData<List<CompletedTripData>>,
                                  val context: Context?,val ridesViewModel: RidesViewModel) : RecyclerView.Adapter<SpecialTripRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                RidersSpecialTripRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] , context!!,ridesViewModel)



    }


    class ViewHolder(private var binding: RidersSpecialTripRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CompletedTripData, context: Context, ridesViewModel: RidesViewModel ) {
            binding.model = item
            binding.executePendingBindings()

            binding.card.setOnClickListener {
                cancelDialog(context,ridesViewModel)
            }

        }

        fun cancelDialog(context: Context,ridesViewModel: RidesViewModel)
        {
            val dialog = Dialog(context)

            dialog.setContentView(R.layout.dialog_cancel)

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            dialog.confirm_btn.setOnClickListener {
                dialog.dismiss()
            }

            dialog.no_btn.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        }

    }


}