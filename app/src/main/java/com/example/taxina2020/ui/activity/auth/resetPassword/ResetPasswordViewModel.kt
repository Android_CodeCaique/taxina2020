package com.example.taxina2020.ui.activity.auth.resetPassword

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ResetPasswordViewModel : BaseViewModel() {
    var code = MutableLiveData<String>("")
    var password = MutableLiveData<String>("")
    var confirmPassword = MutableLiveData<String>("")
    var loader = MutableLiveData<Boolean>(false)

    fun resetPassword(v: View) {
        if (password.value!! != confirmPassword.value)
            Toast.makeText(v.rootView.context, "Password Not Matches", Toast.LENGTH_SHORT).show()
        else {
            if (code.value!!.isNotEmpty() && password.value!!.isNotEmpty() && confirmPassword.value!!.isNotEmpty()) {

                loader.value = true
                val service = ApiClient.makeRetrofitServiceHome()
                CoroutineScope(Dispatchers.IO).launch {

                    val response = service.resetPassword(code.value!!,password.value!!)
                    withContext(Dispatchers.Main) {
                        try {

                            if (response.isSuccessful && response.body()!!.error == 0) {
                                loader.value = false
                                code.value = ""
                                password.value = ""
                                confirmPassword.value = ""
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                                Navigation.findNavController(v).navigate(R.id.action_forgetPasswordFragment_to_resetPasswordFragment)
                            } else if (response.isSuccessful) {
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                loader.value = false
                            } else {
                                Log.i("ForgetPasswordError", response.message())

                            }
                        } catch (e: Exception) {
                            Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                            loader.value = false

                        }
                    }
                }

            } else {
                Toast.makeText(v.rootView.context, "All Field Required ", Toast.LENGTH_SHORT).show()
            }
        }
    }

}