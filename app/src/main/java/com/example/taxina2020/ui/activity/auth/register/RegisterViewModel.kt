package com.example.taxina2020.ui.activity.auth.register

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.google.firebase.iid.FirebaseInstanceId
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.math.log

class RegisterViewModel : BaseViewModel(){

    var fName = MutableLiveData<String>("")
    var lName = MutableLiveData<String>("")
//    var phone = MutableLiveData<String>("")
    var password = MutableLiveData<String>("")
    var confirmPasssword = MutableLiveData<String>("")
    var email = MutableLiveData<String>("")
    var gender = MutableLiveData<Int>(0)
    var loader = MutableLiveData<Boolean>(false)
    var sharedHelper: SharedHelper = SharedHelper()

    var countriesList = ArrayList<String>()
    var countryId = MutableLiveData<Int>(0)

    fun getCountries(context:Context)
    {
        loader.value = true
        val service = ApiClient.makeUserAPIService()
        Log.i("Register", "1")
        CoroutineScope(Dispatchers.IO).launch {
            Log.i("Register", "2")
            val response = service.showCountries()
            Log.i("Response1", response.message())
            Log.i("Response2", response.code().toString())
//                    Log.i("Response3", response.body()!!.message_en)
            withContext(Dispatchers.Main) {
                try {
                    Log.i("Register", "3")

                    if (response.isSuccessful && response.body()!!.error == 0) {

                        loader.value = false

                        for (i in response.body()!!.data)
                        {
                            countriesList.add(i.name)
                        }


                    } else if(response.isSuccessful){
                        loader.value = false
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()

                    }

                    else {
                        Log.i("RegisterError", response.message())
                        loader.value = false
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()

                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false

                }
            }
        }
    }

    fun register(v:View , fNameET : EditText , lNameET : EditText , emailET : EditText , passwordET: EditText)
    {
        if(password.value!! != confirmPasssword.value!! )
        {
            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.passwordNotMatch),Toast.LENGTH_SHORT).show()
            return
        }
        if(fName.value!!.isEmpty())
        {
            fNameET.error = v.rootView.context.resources.getString(R.string.firstNameRequied)
            return
        }
        if(lName.value!!.isEmpty())
        {
            lNameET.error = v.rootView.context.resources.getString(R.string.lastNameRequied)
            return

        }
        if(email.value!!.isEmpty() )
        {
            emailET.error = v.rootView.context.resources.getString(R.string.emailRequied)
            return

        }
        if(!email.value!!.contains("@") || !email.value!!.contains("."))
        {
            emailET.error = v.rootView.context.resources.getString(R.string.emailFormatIsWrong)
            return

        }
        if(password.value!!.isEmpty())
        {
            passwordET.error = v.rootView.context.resources.getString(R.string.passwordRequied)
            return

        }
        else {
            if (fName.value!!.isNotEmpty() && lName.value!!.isNotEmpty() && email.value!!.isNotEmpty()  && password.value!!.isNotEmpty() && countryId.value != 0) {

                loader.value = true
                val service = ApiClient.makeRetrofitService()
                Log.i("Register", "1")
                CoroutineScope(Dispatchers.IO).launch {
                    Log.i("Register", "2")
                    val token :String? = FirebaseInstanceId.getInstance().token
                    val response = service.register(fName.value!!, lName.value!!, email.value!!, sharedHelper.getKey(v.rootView.context,"VerificationPhone").toString(), gender.value!!, password.value!!, "0", "0", token+"",countryId.value!!.toString())
                    Log.i("Response1", response.message())
                    Log.i("Response2", response.code().toString())
//                    Log.i("Response3", response.body()!!.message_en)
                    withContext(Dispatchers.Main) {
                        try {
                            Log.i("Register", "3")

                            if (response.isSuccessful && response.body()!!.error == 0) {
                                Log.i("Register", "4")
                                Log.i("Register", response.message())
                                loader.value = false

                                fName.value = ""
                                lName.value = ""
                                email.value = ""
//                                phone.value = ""
                                password.value = ""
                                confirmPasssword.value = ""
                                sharedHelper.putKey(v.context, "TokenRegister", response.body()!!.data.token)
                                sharedHelper.putKey(v.rootView.context, "OPEN", "OPEN")

                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                Navigation.findNavController(v).navigate(R.id.action_signUp_to_addCarFragment)
                            } else if(response.isSuccessful){
                                loader.value = false
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()

                            }

                            else {
                                Log.i("RegisterError", response.message())
                                loader.value = false
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()

                            }
                        } catch (e: Exception) {
                            Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                            loader.value = false

                        }
                    }
                }
            } else {
                Toast.makeText(v.rootView.context, "All Field Required ", Toast.LENGTH_SHORT).show()
            }
        }
    }
}