package com.example.taxina2020.ui.activity.response

data class NotificationResponse(
    val data: List<NotificationData>,
    val error: Int,
    val message: String
)

data class NotificationData(
    val body: String,
    val click_action: String,
    val created_at: String,
    val id: Int,
    val redirect_id: String,
    val status: String,
    val title: String,
    val type: String,
    val updated_at: String,
    val user_id: String
)