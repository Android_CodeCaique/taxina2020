package com.example.taxina2020.ui.activity.all.online.setting

import ApiClient
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.taxina2020.ui.activity.auth.MainActivity
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class SettingsViewModel : BaseViewModel() {
    var loader = MutableLiveData<Boolean>(false)
    var sharedHelper: SharedHelper = SharedHelper()
    var lang = MutableLiveData<String>("")

    var fName = MutableLiveData<String>("")
    var lName = MutableLiveData<String>("")
    var image = MutableLiveData<String>("")
    var email = MutableLiveData<String>("")

    var newPassword = MutableLiveData<String>("")

    fun changeLang(context: Context) {
        loader.value = true

        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {
            val response = service.changeLanguage("Bearer ${sharedHelper.getKey(context, "Token")}",lang.value!!)
            withContext(Dispatchers.Main) {
                try {
                    Log.i("response1",response.message())
                    if (response.isSuccessful && response.body()!!.error == 0) {
                        loader.value = false

                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        Log.i("response2",response.message())
                        sharedHelper.putKey(context, "MyLang", lang.value.toString())

                        val intent = Intent(context, MainActivity::class.java)
                        (context as Activity).finish()
                        context.startActivity(intent)

                    } else {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                        Log.i("response3",response.message())

                    }
                }catch (e:Exception)
                {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false
                    Log.i("response4",response.message())

                }
            }
        }

    }


    fun getUser(context: Context)
    {
        loader.value = true
        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {
            val response = service.getUserData("Bearer ${sharedHelper.getKey(context, "Token")}", sharedHelper.getKey(context, "UserId")?.toInt()!!)
            withContext(Dispatchers.Main) {
                try {
                    Log.i("response1",response.message())
                    if (response.isSuccessful && response.body()!!.error == 0) {
                        loader.value = false
                        fName.value = response.body()!!.data.first_name
                        lName.value = response.body()!!.data.last_name
                        email.value = response.body()!!.data.email
                        image.value = response.body()!!.data.image

                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        Log.i("response2",response.message())


                    } else {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                        Log.i("response3",response.message())

                    }
                }catch (e:Exception)
                {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false
                    Log.i("response4",response.message())

                }
            }
        }
    }

    fun logout(v: View)
    {
        sharedHelper.putKey(v.rootView.context,"Token","")
        sharedHelper.putKey(v.rootView.context, "NotificationIdTrip", "")
        sharedHelper.putKey(v.rootView.context, "NotificationUserID", "")
        sharedHelper.putKey(v.rootView.context, "OPEN", "OPEN")

        val intent = Intent(v.context, MainActivity::class.java)
        v.context.startActivity(intent)
        (v.context as Activity).finish()
//        (v.rootView.context as Activity ).finish()
    }

    fun changePassword(context: Context)
    {
        loader.value = true

        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {
            val response = service.changePassword("Bearer ${sharedHelper.getKey(context, "Token")}",newPassword.value!!)
            withContext(Dispatchers.Main) {
                try {
                    Log.i("response1",response.message())
                    if (response.isSuccessful && response.body()!!.error == 0) {
                        loader.value = false

                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()


                    } else {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                        Log.i("response3",response.message())

                    }
                }catch (e:Exception)
                {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false
                    Log.i("response4",response.message())

                }
            }
        }
    }


}