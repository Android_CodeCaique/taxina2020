package com.example.taxina2020.ui.activity.auth.viecleType

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentViecleTypeBinding

/**
 * A simple [Fragment] subclass.
 */
class ViecleType : Fragment() {

    lateinit var binding:  FragmentViecleTypeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = FragmentViecleTypeBinding.inflate(inflater)
        binding.lifecycleOwner = this



        return binding.root
    }

}
