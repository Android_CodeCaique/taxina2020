package com.example.taxina2020.ui.activity.response

data class AllCountryResponse(
    val `data`: List<AllCountryData>,
    val error: Int,
    val message: String
)

data class AllCountryData(
    val created_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)