package com.example.taxina2020.ui.activity.all.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.taxina2020.R;

import static com.example.taxina2020.ui.activity.all.chat.Chat.currantFrag;


public class ProfileFromMicrobus5_1_1 extends Fragment {

    ImageButton menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        currantFrag = R.layout.fragment_online_microbus_details5_1;
        View view = inflater.inflate(R.layout.fragment_profile_microbus5_1_1, container, false);
        menu = getActivity().findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);
        return view;
    }

}
