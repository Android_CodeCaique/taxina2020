package com.example.taxina2020.ui.activity.response

data class UserSeatsResponse(
    val data: List<UserSeatsData>,
    val error: Int,
    val message: String
)

data class UserSeatsData(
    val id: Int,
    val is_ride: String,
    val money: String,
    val seat_num: String
)