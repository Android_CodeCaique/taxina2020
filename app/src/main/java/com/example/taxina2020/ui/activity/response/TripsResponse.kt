package com.example.taxina2020.ui.activity.response

data class TripsResponse(
        val created_at: String,
        val date: String,
        val distance: String,
        val driver_id: String,
        val end_lat: String,
        val end_location: String,
        val end_long: String,
        val first_name: String,
        val id: Int,
        val image: String,
        val last_name: String,
        val note: Any,
        val payment_method: String,
        val price: Any,
        val promo_code: Any,
        val rate: String,
        val start_lat: String,
        val start_location: String,
        val start_long: String,
        val status: String,
        val time: String,
        val type: String,
        val userCar_id: String,
        val user_id: String
)