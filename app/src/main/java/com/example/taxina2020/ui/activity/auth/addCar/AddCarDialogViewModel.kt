//package com.example.taxina2020.ui.activity.auth.addCar
//
//import android.content.Context
//import android.util.Log
//import android.widget.Toast
//import androidx.lifecycle.MutableLiveData
//import com.example.taxina2020.ui.activity.utils.SharedHelper
//import com.shoohna.shoohna.util.base.BaseViewModel
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.async
//import kotlinx.coroutines.withContext
//
//class AddCarDialogViewModel : BaseViewModel() {
//
//
//    var loader = MutableLiveData<Boolean>(false)
//    var sharedHelper: SharedHelper = SharedHelper()
//
//    fun getSuggestedCar(context: Context)
//    {
//
//        loader.value = true
//        val service = ApiClient.makeRetrofitService()
//        CoroutineScope(Dispatchers.IO).async {
//
//            val response = service.showSuggestedCar("Bearer ${sharedHelper.getKey(context, "Token")}",carType.value!!)
//
//            withContext(Dispatchers.Main) {
//                try {
//
//                    if (response.isSuccessful && response.body()!!.error == 0) {
//                        for (i in response.body()!!.data) {
//                            carTypeListRespoName.add(i.name)
//                            carTypeListRespoId.add(i.id)
//                        }
//
//                        Log.i("Response",response.body()!!.data.toString())
//                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                        loader.value = false
//
//                    } else {
//
//                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
//                        loader.value = false
//                    }
//                } catch (e: Exception) {
//                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
//                    loader.value = false
//
//                }
//            }
//        }
//    }
//
//}