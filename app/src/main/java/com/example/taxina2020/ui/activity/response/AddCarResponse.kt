package com.example.taxina2020.ui.activity.response

data class AddCarResponse(
    val `data`: AddCarData,
    val error: Int,
    val message: String
)

data class AddCarData(
    val Latitude: String,
    val Longitude: String,
    val code: String,
    val created_at: String,
    val email: String,
    val firebase_token: Any,
    val first_name: String,
    val gender: String,
    val id: Int,
    val image: String,
    val is_busy: String,
    val is_driver: String,
    val is_online: String,
    val last_name: String,
    val phone: String,
    val rate: String,
    val role: String,
    val target: String,
    val token: String,
    val updated_at: String,
    val wallet: String
)