package com.example.taxina2020.ui.activity.response

data class AreaByIdResponse(
    val data: List<AreaByIdData>,
    val error: Int,
    val message: String
)

data class AreaByIdData(
    val city_id: String,
    val created_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)