package com.example.taxina2020.ui.activity.response

data class WalletResponse(
    val data: WalletData,
    val error: Int,
    val message: String
)

data class WalletData(
    val driver_wallet: String,
    val target: String,
    val total_earned: String,
    val trip_count: String
)