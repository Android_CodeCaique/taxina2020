package com.example.taxina2020.ui.activity.rides

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.taxina2020.ui.activity.response.CompletedTripData
import com.example.taxina2020.ui.activity.response.UpcomingBusData
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class RidesViewModel : BaseViewModel() {

    var loader = MutableLiveData<Boolean>(false)
    var sharedHelper: SharedHelper = SharedHelper()
    var trips: MutableLiveData<List<CompletedTripData>> = MutableLiveData()
    var trip_count = MutableLiveData<String>("")
    var earned = MutableLiveData<String>("")

    val TAG = "RidesViewModel"

    var speacialTrips: MutableLiveData<List<CompletedTripData>> = MutableLiveData()


    var speacial_trip_count = MutableLiveData<String>("")
    var speacialEarned = MutableLiveData<String>("")

    var speacial_trip_count_bus_completed = MutableLiveData<String>("")
    var speacialEarned_bus_completed = MutableLiveData<String>("")



    var earnedCompleted = MutableLiveData<String>("")
    var countCompleted = MutableLiveData<String>("")

    var earnedUpcoming = MutableLiveData<String>("")
    var countUpcoming = MutableLiveData<String>("")


    var usersBusUpcoming: MutableLiveData<List<UpcomingBusData>> = MutableLiveData()
    var usersBusCompleted: MutableLiveData<List<UpcomingBusData>> = MutableLiveData()


    // Car Driver Rides

    fun getCompletedTrips(context: Context)
    {
                loader.value = true
                val service = ApiClient.makeRetrofitServiceHome()
                CoroutineScope(Dispatchers.IO).launch {

                    val response = service.completedTrip("Bearer ${sharedHelper.getKey(context, "Token")}")

                    withContext(Dispatchers.Main) {
                        try {

                            if (response.isSuccessful && response.body()!!.error == 0) {

                                loader.value = false
//                                      Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                trips.value = response.body()!!.data
                                countCompleted.value = response.body()!!.trip_count
                                earnedCompleted.value = response.body()!!.earned
                                Log.i("CountCompleteCar",response.body()!!.trip_count)
                                Log.i("earnedCompleteCar",response.body()!!.earned)
                            } else if (response.isSuccessful) {
                                Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                loader.value = false
                            } else {
                                Log.i("RegisterError", response.message())
                                loader.value = false

                            }
                        } catch (e: Exception) {
                            Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                            loader.value = false

                        }
                    }
        }
    }

    fun getSpecialTrips(context: Context)
    {
        loader.value = true
        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).launch {

            val response = service.getTrip("Bearer ${sharedHelper.getKey(context, "Token")}")

            Log.e(TAG, "getSpecialTrips: " + sharedHelper.getKey(context, "Token") )

            withContext(Dispatchers.Main) {
                try {

                    if (response.isSuccessful && response.body()!!.error == 0) {

                        loader.value = false
//                                      Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        speacialTrips.value = response.body()!!.data
                        countCompleted.value = response.body()!!.trip_count
                        earnedCompleted.value = response.body()!!.earned
                    } else if (response.isSuccessful) {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                    } else {
                        Log.i("RegisterError", response.message())
                        loader.value = false

                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false

                }
            }
        }
    }


    fun acceptCancelTrip(context: Context, tripId: String, accept: String){

        val service = ApiClient.makeRetrofitServiceHome()

        CoroutineScope(Dispatchers.IO).launch {
            val response  =  service.driverAcceptCancelTrip(tripId,accept)
            Log.e(TAG, "RequestCancelTrip: ")

            withContext(Dispatchers.Main)
            {
                try {

                }
                catch (e: Exception) {
                    Log.e(TAG, "RequestCancelTrip: ${e.message}   " )
                }
            }

        }

    }


    // Bus Driver Rides

    fun getBusSpecialTrips(context: Context)
    {
        loader.value = true
        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {

            val response = service.upcomingBus("Bearer ${sharedHelper.getKey(context, "Token")}")

            withContext(Dispatchers.Main) {
                try {

                    if (response.isSuccessful && response.body()!!.error == 0) {
                        usersBusUpcoming.value = response.body()!!.data
                        countCompleted.value = response.body()!!.trip_count
                        earnedCompleted.value = response.body()!!.earned
                        Log.i("Response",response.body()!!.data.toString())
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false

                    } else {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false

                }
            }
        }
    }

    fun getBusCompletedTrips(context: Context)
    {
        loader.value = true
        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {

            val response = service.completedBus("Bearer ${sharedHelper.getKey(context, "Token")}")

            withContext(Dispatchers.Main) {
                try {

                    if (response.isSuccessful && response.body()!!.error == 0) {
                        usersBusCompleted.value = response.body()!!.data
                        countCompleted.value = response.body()!!.trip_count
                        earnedCompleted.value = response.body()!!.earned

                        Log.i("Response",response.body()!!.data.toString())
                        Log.i("count_bus_completed",response.body()!!.trip_count.toString())
                        Log.i("Earned_bus_completed",response.body()!!.earned.toString())
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false

                    } else {
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false

                }
            }
        }
    }
}