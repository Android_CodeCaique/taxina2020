package com.example.taxina2020.ui.activity.response

data class CompletedTripBusResponse(
    val data: List<CompletedTripBusData>,
    val error: Int,
    val message: String
)

data class CompletedTripBusData(
    val created_at: String,
    val date: String,
    val end_Latitude: String,
    val end_Longitude: String,
    val end_location: String,
    val from_time: String,
    val id: Int,
    val is_driver: String,
    val price: String,
    val seat_count: String,
    val start_Latitude: String,
    val start_Longitude: String,
    val start_location: String,
    val state: String,
    val to_time: String,
    val updated_at: String,
    val user_id: String
)