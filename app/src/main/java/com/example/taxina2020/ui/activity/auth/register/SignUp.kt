package com.example.taxina2020.ui.activity.auth.register

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentSignUpBinding
import com.example.taxina2020.ui.activity.all.Main2Activity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.json.JSONException
import org.json.JSONObject
import java.net.MalformedURLException
import java.net.URL

class SignUp : Fragment() {
    var binding: FragmentSignUpBinding? = null
    var registerViewModel: RegisterViewModel? = null
    var callbackManager : CallbackManager?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentSignUpBinding.inflate(inflater)
        binding!!.lifecycleOwner = this
        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        binding!!.vm = registerViewModel
        binding!!.face.setOnClickListener {
            clickOnFaceBook()
        }
        binding!!.google.setOnClickListener {
            clickOnGoogle()
        }

        registerViewModel!!.getCountries(requireContext())
        binding!!.countrySpinner.setItems(registerViewModel!!.countriesList)
        binding!!.countrySpinner.setOnItemSelectedListener { view, position, id, item ->
//            Toast.makeText(requireContext(), "Position : $position",Toast.LENGTH_SHORT).show()
            registerViewModel!!.countryId.value = position+1
        }
        return binding!!.root
    }

    private fun clickOnFaceBook ()
    {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
                .logInWithReadPermissions(this, listOf("email", "public_profile"))
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult?) {
                        //To change body of created functions use File | Settings | File Templates.
                        // Here we will add to get photo , email
                        // App code
                        val UserId: String = loginResult!!.accessToken.userId

                        val graphRequest = GraphRequest.newMeRequest(
                                loginResult!!.accessToken
                        ) { `object`, _ -> getData(`object`) }
                        val parameters = Bundle()
                        parameters.putString("fields", "first_name , last_name , email , id")
                        graphRequest.parameters = parameters
                        graphRequest.executeAsync()
                    }

                    override fun onError(error: FacebookException?) {
                        //To change body of created functions use File | Settings | File Templates.
                        Log.d("Here", "Here 1")
                    }

                    override fun onCancel() {
                        //To change body of created functions use File | Settings | File Templates.
                        Log.d("Here", "Here 2")
                    }
                })
    }

    private fun getData(`object`: JSONObject) {
        var first_name: String?
        var last_name: String?
        var email: String?
        val id: String
        first_name = ""
        last_name = ""
        email = ""
        val url: URL
        try {
            first_name = `object`.getString("first_name")
            last_name = `object`.getString("last_name")
            email = `object`.getString("email")
            id = `object`.getString("id")
            url =
                    URL("https://graph.facebook.com/" + `object`.getString("id") + "/picture?width=250&height=250")
            // LoginFBFunction(first_name+" "+last_name , email , id , url.toString());
//            loginViewModel.login(requireView(),email)
//            loginViewModel.login(requireContext(),email)

            Toast.makeText(requireContext(),email,Toast.LENGTH_SHORT).show()
            Log.d("IMAGE", url.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(requireContext(),e.message,Toast.LENGTH_SHORT).show()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            Toast.makeText(requireContext(),e.message,Toast.LENGTH_SHORT).show()

        }
    }

    private fun  handleSignInResult( completedTask : Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val email  : String? = account!!.email
//            loginViewModel.login(requireContext(),email)
        } catch ( e :Exception) {

        }
    }

    private fun clickOnGoogle()
    {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        val  mGoogleSignInClient = GoogleSignIn.getClient(requireView().context, gso)

        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, 1)

        val account = GoogleSignIn.getLastSignedInAccount(requireView().context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.tvLogin.setOnClickListener { v -> Navigation.findNavController(v).navigate(R.id.action_signUp_to_login) }
        binding!!.genderSpinner.setItems("Male", "Female")
        binding!!.genderSpinner.setOnItemSelectedListener { view, position, id, item ->
//            Toast.makeText(activity, position.toString(), Toast.LENGTH_LONG).show()
            registerViewModel!!.gender.value = position
            //                registerViewModel.gender.setValue("Male");
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            val intent = Intent(context, Main2Activity::class.java)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        else
        {
            callbackManager?.onActivityResult(requestCode , resultCode , data)

        }
    }
}