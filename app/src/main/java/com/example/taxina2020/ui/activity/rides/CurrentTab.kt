package com.example.taxina2020.ui.activity.rides

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentCurrentTapBinding
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CurrentTab : Fragment(), CoroutineScope {

    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    private lateinit var binding: FragmentCurrentTapBinding
    private lateinit var viewModel: CurrentTabViewModel
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_tap, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(CurrentTabViewModel::class.java)
        viewModel.mBinding.value = binding
        binding.vm = viewModel
        navController = NavController(requireContext())

        launch {
            viewModel.getCurrentTrip(requireContext())
        }
        viewModel.currentTripLiveData.observe(viewLifecycleOwner, Observer {

            binding.currentTab.tvNots.text = it.start_location
            binding.currentTab.tv4.text = it.end_location
            binding.currentTab.tvDistance.text = it.distance

            Picasso.get().load(it.image).into(binding.currentTab.driver)
            binding.currentTab.tvName.text = it.first_name + " " + it.last_name
            binding.currentTab.tvPrice.text = it.price.toString() + " " + getString(R.string.eg)

        })

        binding.currentTab.root.setOnClickListener {

            navController.navigate(R.id.rideDetails)

        }

    }
}

