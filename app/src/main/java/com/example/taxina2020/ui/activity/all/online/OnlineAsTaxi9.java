package com.example.taxina2020.ui.activity.all.online;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.taxina2020.R;

import static com.example.taxina2020.ui.activity.all.chat.Chat.currantFrag;
import static com.example.taxina2020.ui.activity.all.online.OnlineAsTaxi6.driverIs;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnlineAsTaxi9 extends Fragment {

    ConstraintLayout conOffline, conOnline;
    Button btn_drop_off;
    ImageButton menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online_as_taxi9, container, false);
        initiation(view);


        TextView tv_offline = view.findViewById(R.id.tv_offline);
        tv_offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conOffline.setVisibility(View.GONE);
                conOnline.setVisibility(View.VISIBLE);

            }
        });

        TextView tv_online = view.findViewById(R.id.tv_online);
        tv_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conOnline.setVisibility(View.GONE);
                conOffline.setVisibility(View.VISIBLE);

            }
        });


        btn_drop_off = view.findViewById(R.id.btn_drop_off);
        btn_drop_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Payment10 payment10 = new Payment10();
////                FragmentManager manager = getFragmentManager();
////                manager.beginTransaction().replace(R.id.relativ1, payment10).commit();
//                Navigation.findNavController(v).navigate(R.id.action_onlineAsTaxi9_to_payment10);
            }
        });

        return view;
    }

    private void initiation(View view) {
        menu = getActivity().findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);

        conOffline = view.findViewById(R.id.conOffline);
        conOnline = view.findViewById(R.id.conOnline);

        if (driverIs.equals("taxi")) {
            currantFrag = R.layout.fragment_online_as_taxi8;
        } else
            currantFrag = R.layout.fragment_online_microbus_details5_1;

    }
}


