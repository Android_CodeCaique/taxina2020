package com.example.taxina2020.ui.activity.networking

import com.example.taxina2020.ui.activity.response.*
import retrofit2.Response
import retrofit2.http.*

interface Auth {

    @FormUrlEncoded
    @POST("Driver_registration")
    suspend fun register(@Field("first_name") fName :String ,
    @Field("last_name") lName:String ,
    @Field("email") email:String ,
    @Field("phone") phone :String ,
    @Field("gender") gender:Int ,
    @Field("password") password:String,
    @Field("Latitude") Latitude :String,
    @Field("Longitude") Longitude:String,
                         @Field("firebase_token") firebase_token:String,@Field("country_id") country_id:String) : Response<RegisterResponse>


    @FormUrlEncoded
    @POST("Driver_login")
    suspend fun login(@Field("email") email:String , @Field("password") password:String,@Field("firebase_token") firebase_token:String) : Response<LoginResponse>

    @FormUrlEncoded
    @POST("add_car")
    suspend fun addCar(@Header("Authorization") Authorization:String ,
                       @Field("type_id") type_id:Int,
                       @Field("carCat_id") carCat_id:Int ,
                       @Field("car_name") car_name:String ,
                       @Field("model") model:String ,
                       @Field("plate") plate:String ,
                       @Field("color")  color:String ,
                       @Field("year") year:String,
                       @Field("driver_license") driver_license:String) : Response<AddCarResponse>
//@Field("driver_license") driver_license:String


    @GET("show_suggested_car")
    suspend fun showSuggestedCar(@Header("Authorization") Authorization:String , @Query("type_id") type_id:Int) : Response<ShowSuggestedCarResponse>

    @GET("check_phone")
    suspend fun checkPhone(@Query("phone") phone: String) : Response<BaseResponse>
}