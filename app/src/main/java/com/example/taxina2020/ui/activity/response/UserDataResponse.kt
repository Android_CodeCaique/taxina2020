package com.example.taxina2020.ui.activity.response

data class UserDataResponse(
    val data: UserDataData,
    val error: Int,
    val message: String
)

data class UserDataData(
    val Latitude: String,
    val Longitude: String,
    val email: String,
    val firebase_token: Any,
    val first_name: String,
    val gender: String,
    val id: Int,
    val image: String,
    val is_busy: String,
    val is_driver: String,
    val is_online: String,
    val last_name: String,
    val phone: String,
    val rate: String,
    val role: String
)