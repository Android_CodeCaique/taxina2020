package com.example.taxina2020.ui.activity.response

data class ShowTripSeatsResponse(
    val `data`: ShowTripSeatsData,
    val error: Int,
    val message: String
)

data class ShowTripSeatsData(
    val avaliable_num: Int,
    val block: Int,
    val car_name: String,
    val color: String,
    val date: String,
    val end_Latitude: String,
    val end_Longitude: String,
    val end_location: String,
    val from_time: String,
    val id: Int,
    val plate: String,
    val price: String,
    val ride_num: Int,
    val seat_count: String,
    val seats: List<ShowTripSeatsSeat>,
    val start_Latitude: String,
    val start_Longitude: String,
    val start_location: String,
    val to_time: String
)

data class ShowTripSeatsSeat(
    val id: Int,
    val is_ride: String,
    val seat_num: String
)