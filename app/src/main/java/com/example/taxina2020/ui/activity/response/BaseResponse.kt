package com.example.taxina2020.ui.activity.response

data class BaseResponse(
    val error: Int,
    val message: String
)