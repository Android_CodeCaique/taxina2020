package com.example.taxina2020.ui.activity.rides

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.taxina2020.databinding.LayoutProcessOneBinding
import com.example.taxina2020.databinding.RidersCompletedRowBinding
import com.example.taxina2020.ui.activity.response.CompletedTripData

class CompletedTripRecyclerAdapter (private var dataList: LiveData<List<CompletedTripData>>,
                                    private val context: Context?) : RecyclerView.Adapter<CompletedTripRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                RidersCompletedRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] )


    }


    class ViewHolder(private var binding: RidersCompletedRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CompletedTripData ) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}