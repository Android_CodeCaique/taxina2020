package com.example.taxina2020.ui.activity.rides

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.taxina2020.databinding.*
import com.example.taxina2020.ui.activity.response.UpcomingBusData

class SpecialTripBusRecyclerAdapter (private var dataList: LiveData<List<UpcomingBusData>>,
                                     private val context: Context?) : RecyclerView.Adapter<SpecialTripBusRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(UpcomingBusRidesRowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] )
    }


    class ViewHolder(private var binding: UpcomingBusRidesRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UpcomingBusData) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}