package com.example.taxina2020.ui.activity.all.online;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taxina2020.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnlineAsTaxi6 extends Fragment {

    public static String driverIs;
    ConstraintLayout conOffline, conOnline;
    RecyclerView rv_passenger;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online_as_taxi6, container, false);
        // currantFrag = R.layout.fragment_online_as_taxi6;
        driverIs = "taxi";
        conOffline = view.findViewById(R.id.conOffline);
        conOnline = view.findViewById(R.id.conOnline);

        TextView tv_offline = view.findViewById(R.id.tv_offline);
        tv_offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conOffline.setVisibility(View.GONE);
                conOnline.setVisibility(View.VISIBLE);

            }
        });

        TextView tv_online = view.findViewById(R.id.tv_online);
        tv_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conOnline.setVisibility(View.GONE);
                conOffline.setVisibility(View.VISIBLE);

            }
        });

        rv_passenger = view.findViewById(R.id.rv_passenger);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.HORIZONTAL, false);
        PassengerRecyclerViewAdabter passengerRecyclerViewAdabter = new PassengerRecyclerViewAdabter(view.getContext());
        rv_passenger.setAdapter(passengerRecyclerViewAdabter);
        rv_passenger.setLayoutManager(linearLayoutManager);

        return view;
    }
}
