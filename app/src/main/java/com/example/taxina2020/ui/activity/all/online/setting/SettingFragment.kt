package com.example.taxina2020.ui.activity.all.online.setting

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentSettingBinding
import com.example.taxina2020.ui.activity.utils.LanguageHelper
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class SettingFragment : Fragment() {

    lateinit var binding : FragmentSettingBinding
    val languageHelper = LanguageHelper ()

    lateinit var settingViewModel : SettingsViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = FragmentSettingBinding.inflate(inflater)
        binding.lifecycleOwner = this
        settingViewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        binding.vm = settingViewModel
        settingViewModel.getUser(requireContext())
        binding.profileConId.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_settingFragment_to_profileFragment)
        }

        binding.passwordUserId.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
//                model.searchByName(activity!!,binding.searchEditTextID)
                if(binding.passwordUserId.text.isEmpty())
                    Toast.makeText(requireActivity(),"Please write new password",Toast.LENGTH_SHORT).show()
                else {
                    settingViewModel.changePassword(requireContext())
                    binding.passwordUserId.text.clear()
                }
                return@OnKeyListener true
            }
            false
        })
        handleLanguageSpinner()

        return binding.root
    }

    private fun handleLanguageSpinner() {
        try {
            binding.spinner.setItems("Language's", "English", "العربيه")
            binding.spinner.setOnItemSelectedListener { view, position, id, item ->
                if (position == 1) {
                    Log.i("Lang", "1")
//                        setLocate("en")
                        languageHelper.ChangeLang(this.resources , "en")

                    settingViewModel.lang.value = "en"
                    settingViewModel.changeLang(requireActivity())

                }
                if (position == 2) {

//                        setLocate("ar")
                        languageHelper.ChangeLang(this.resources , "ar")

                    settingViewModel.lang.value = "ar"
                    settingViewModel.changeLang(requireActivity())

                }
            }
        }catch (e: Exception) {
            Toast.makeText(requireActivity(), e.message, Toast.LENGTH_SHORT).show()
        }
    }

}
