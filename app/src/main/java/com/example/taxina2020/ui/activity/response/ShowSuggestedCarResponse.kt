package com.example.taxina2020.ui.activity.response

data class ShowSuggestedCarResponse(
    val data: List<ShowSuggestedCarData>,
    val error: Int,
    val message: String
)

data class ShowSuggestedCarData(
    val id: Int,
    val name: String
)