package com.example.taxina2020.ui.activity.roomDB

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.taxina2020.ui.activity.networking.StepsDao


@Database(entities = [StepsModel::class], version = 2)
abstract class AppDatabase : RoomDatabase(){
    abstract fun stepsDao(): StepsDao

}