package com.example.taxina2020.ui.activity.auth.verifyPhone

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentVerifyPhoneBinding
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import java.util.concurrent.TimeUnit


class VerifyPhone : Fragment() {

    lateinit var binding : FragmentVerifyPhoneBinding
    lateinit var sharedHelper: SharedHelper
    lateinit var mVerificationId : String
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentVerifyPhoneBinding.inflate(inflater)
        binding.lifecycleOwner = this
        sharedHelper = SharedHelper()

        binding.btnVerifyCode.setOnClickListener {
            verifyVerificationCode(requireContext(),binding.codeId.otp.toString())
        }


//        binding.btnVerifyCode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifyVerificationCode(requireContext(), Objects.requireNonNull(binding.codeId.getOtp()));
//            }
//        });
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                sharedHelper.getKey(requireContext(), "VerificationPhone")!!,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks)

        binding.codeId.otp

        return binding.root
    }

    private fun verifyVerificationCode(context:Context, code: String) {
        //creating the credential
        if (code.isEmpty())
            Toast.makeText(requireContext(), "Enter code !", Toast.LENGTH_SHORT).show()
        else {
            binding.mkLoaderId.visibility = View.VISIBLE

            val sharedHelper = SharedHelper()
//            val verificationId: String? = sharedHelper.getKey(context, "ActiveCode")
            val auth = FirebaseAuth.getInstance();
            val credential = PhoneAuthProvider.getCredential(mVerificationId, code)
            auth.signInWithCredential(credential)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d("D", "1")
                            binding.mkLoaderId.visibility = View.INVISIBLE
                            Navigation.findNavController(requireView()).navigate(R.id.action_verifyPhone_to_signUp)
                        } else {
                            Log.d("D", "2")
                            binding.mkLoaderId.visibility = View.INVISIBLE
                            Toast.makeText(context, "Exception \n" + task.exception?.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    .addOnFailureListener {

                        binding.mkLoaderId.visibility = View.INVISIBLE
                        Toast.makeText(context, "Exception \n" + it.message, Toast.LENGTH_SHORT).show()


                    }
        }
    }

    private val mCallbacks: OnVerificationStateChangedCallbacks = object : OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {

            //Getting the code sent by SMS
            val code = phoneAuthCredential.smsCode

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
//                editTextCode.setText(code);
                //verifying the code
                binding.codeId.setOTP(code)
                verifyVerificationCode(requireContext(), code)
                //                verifyVerificationCode(code);
            }
        }

        override fun onVerificationFailed(e: FirebaseException) {
            Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
        }

        override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken) {
            super.onCodeSent(s, forceResendingToken)

            //storing the verification id that is sent to the user
            mVerificationId = s
        }
    }


}