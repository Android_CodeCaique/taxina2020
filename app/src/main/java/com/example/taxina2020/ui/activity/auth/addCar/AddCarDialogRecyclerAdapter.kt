//package com.example.taxina2020.ui.activity.auth.addCar
//
//import android.content.Context
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import android.widget.Toast
//import androidx.lifecycle.LiveData
//import androidx.recyclerview.widget.RecyclerView
//import com.example.taxina2020.databinding.LocationRideItemBinding
//import com.example.taxina2020.ui.activity.response.AllCityData
//
//class AddCarDialogRecyclerAdapter (private var dataList: LiveData<List<AllCityData>>,
//                                   private val context: Context?, private var createRideViewModel: CreateRideViewModel, var locationType:String) : RecyclerView.Adapter<AddCarDialogRecyclerAdapter.ViewHolder>() {
//
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder(
//                LocationRideItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//        )
//    }
//
//    override fun getItemCount(): Int {
//        return dataList.value!!.size
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        if (context != null) {
//            holder.bind(dataList.value!![position] , createRideViewModel , context , locationType)
//        }
//
//
//    }
//
//
//    class ViewHolder(private var binding: LocationRideItemBinding) : RecyclerView.ViewHolder(binding.root) {
//        fun bind(item: AllCityData , createRideViewModel: CreateRideViewModel , context: Context? , locationType: String) {
//            binding.model = item
//            binding.executePendingBindings()
//            binding.nameId.setOnClickListener {
//                if(locationType == "locationFrom") {
//                    createRideViewModel.locationFrom.value = item.id.toString()
//                    createRideViewModel.locationFromName.value = item.name
//                }else if(locationType == "locationTo")
//                {
//                    createRideViewModel.locationTo.value = item.id.toString()
//                    createRideViewModel.locationToName.value = item.name
//
//                }
//                Toast.makeText(context , "Selected Address "+item.name , Toast.LENGTH_SHORT).show()
//            }
//        }
//
//    }
//
//}