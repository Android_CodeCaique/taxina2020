package com.example.taxina2020.ui.activity.auth.addCar

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.databinding.FragmentAddCarBinding
import com.example.taxina2020.ui.activity.utils.MonthYearPickerDialog
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class AddCarFragment : Fragment() {
    lateinit var binding : FragmentAddCarBinding
    lateinit var addCarViewModel : AddCarViewModel
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)
    var sdf: SimpleDateFormat = SimpleDateFormat("MMM yyyy")
    var input: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentAddCarBinding.inflate(inflater)
        binding.lifecycleOwner = this

        addCarViewModel = ViewModelProviders.of(this).get(AddCarViewModel::class.java)
        binding.vm = addCarViewModel
        binding.carTypeId.setItems("Car","Bus") // 1 Car , 2 Bus
        binding.carTypeId.setOnItemSelectedListener { view, position, id, item ->
            Log.i("Select Brand",position.toString())
            if(position == 0)
               addCarViewModel.carType.value = 1
            else if (position == 1 )
                addCarViewModel.carType.value = 2

            addCarViewModel.getSuggestedCar(requireContext())
            binding.vehicleBrandSpinnerId.getItems<String>().clear()

        }

        binding.vehicleBrandSpinnerId.setItems(addCarViewModel.carTypeListRespoName)
        binding.vehicleBrandSpinnerId.setOnItemSelectedListener { view, position, id, item ->
//            val selectedId =  addCarViewModel.carTypeListRespoId[position + 1]
//            addCarViewModel.vehicleBrand.value = addCarViewModel.carTypeListRespoName[selectedId]
//            addCarViewModel.vehicleBrand.value = position+1
            addCarViewModel.carCatId.value = position
            addCarViewModel.carName.value = addCarViewModel.carTypeListRespoName[position]
            addCarViewModel.carCatIdSelected.value = true

            Log.i("Car Name",addCarViewModel.carTypeListRespoName[position])
            Log.i("Car ID",position.toString())
        }

        binding.yearId.setOnClickListener {
//            val dpd = DatePickerDialog(requireActivity(), DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                binding.yearId.text = year.toString()
//                addCarViewModel.year.value = year.toString()
//            }, year, month, day)
//            dpd.show()

            val pickerDialog = MonthYearPickerDialog()
            pickerDialog.setListener { datePicker, year, month, i2 ->
                val monthYearStr = year.toString() + "-" + (month + 1) + "-" + i2
                Log.i("Year Add Car" , year.toString())
//                click_me.setText(formatMonthYear(monthYearStr))
                binding.yearId.setText(year.toString())
                addCarViewModel.year.value = year.toString()
            }
            pickerDialog.show(activity?.supportFragmentManager!!, "MonthYearPickerDialog")
        }



        return binding.root
    }

    fun formatMonthYear(str: String?): String? {
        var date: Date? = null
        try {
            date = input.parse(str)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return sdf.format(date)
    }

}
