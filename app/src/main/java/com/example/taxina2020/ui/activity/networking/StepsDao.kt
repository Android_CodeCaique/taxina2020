package com.example.taxina2020.ui.activity.networking

import androidx.room.*
import com.example.taxina2020.ui.activity.roomDB.StepsModel

@Dao
interface StepsDao {

    @Query("SELECT * FROM StepsModel")
    fun getAllSteps(): List<StepsModel>

    @Query("SELECT meters FROM StepsModel WHERE user_id = :user_id" )
    fun getAllStepsById(user_id:Int) : Double

//    @Query("SELECT meters FROM StepsModel WHERE user_id = :user_id")
//    fun getAllSelectedMetersFromUser(user_id:Int) : List<StepsModel>

    @Query("UPDATE StepsModel SET meters = :meters WHERE user_id = :user_id")
    fun updateMetersOfUserId(meters:Double , user_id: Int)

    @Query("DELETE FROM StepsModel WHERE user_id = :user_id")
    fun deleteStepsById(user_id:Int)

    @Query("DELETE FROM StepsModel")
    fun deleteAllStepsDatabase()

    @Update
    fun updateSteps(stepsModel: StepsModel)

    @Insert
    fun insertSteps(stepsModel: StepsModel)

    @Delete
    fun deleteSteps(stepsModel: StepsModel)

}