package com.example.taxina2020.ui.activity.response

data class UpcomingBusResponse(
    val data: List<UpcomingBusData>,
    val error: Int,
    val earned: String,
    val trip_count: String,
    val message: String

)

data class UpcomingBusData(
    val car_name: String,
    val color: String,
    val date: String,
    val from_area: String,
    val from_areaId: String,
    val from_time: String,
    val fromcity_name: String,
    val id: Int,
    val plate: String,
    val price: String,
    val seat_count: String,
    val to_area: String,
    val to_areaId: String,
    val to_time: String,
    val tocity_name: String,
    val trip_time:String
)