package com.example.taxina2020.ui.activity.networking

import com.example.taxina2020.ui.activity.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface Home {

    @GET("online_offline")
    suspend fun onlineOffline(@Header("Authorization") Authorization: String): Response<BaseResponse>

    @GET("Driver_requested_trip")
    suspend fun driverRequestedTrip(@Header("Authorization") Authorization: String, @Query("latitude") latitude: Double, @Query("longitude") longitude: Double): Response<DriverRequestedTripResponse>

    @GET("accept_trip")
    suspend fun driveAcceptTrip(@Query("trip_id") trip_id: Int, @Header("Authorization") Authorization: String): Response<AcceptTripResponse>

    @GET("driver_request_cancel_trip")
    suspend fun driverRequestCancelTrip(@Query("trip_id") trip_id: String): Response<ResponseBody>

    @GET("driver_accept_cancel_trip")
    suspend fun driverAcceptCancelTrip(@Query("trip_id") trip_id: String, @Query("accept") accept: String): Response<ResponseBody>


    @GET("json")
    suspend fun getDistanceAndTimeGoogleAPI(@Query("origin") origin: String,
                                            @Query("destination") destination: String,
                                            @Query("key") key: String,
                                            @Query("mode") mode: String): Response<GoogleDirectionAPIResponse>

    @GET("trip_payment")
    suspend fun tripPayment(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int, @Query("paid") paid: Int): Response<BaseResponse>

    @GET("show_driver_completedtrip")
    suspend fun completedTrip(@Header("Authorization") Authorization: String): Response<CompletedTripResponse>

    @GET("show_UpComingdriver_specialTrip")
    suspend fun specialTrip(@Header("Authorization") Authorization: String): Response<CompletedTripResponse>

    @GET("show_UpComingdriver_Trip")
    suspend fun getTrip(@Header("Authorization") Authorization: String): Response<CompletedTripResponse>


    @GET("completedDriver_Bus")
    suspend fun completedTripBus(@Header("Authorization") Authorization: String): Response<CompletedTripResponse>

    @GET("upcomingDriver_Bus")
    suspend fun specialTripBus(@Header("Authorization") Authorization: String): Response<CompletedTripResponse>


    @GET("show_trip_seats")
    suspend fun showTripSeats(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int): Response<ShowTripSeatsResponse>

    @GET("Block_trip")
    suspend fun blockTrip(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int, @Query("seat_id") seat_id: Int): Response<BaseResponse>

    @GET("show_all_city")
    suspend fun showAllCity(@Header("Authorization") Authorization: String, @Query("country_id") country_id: Int): Response<AllCityResponse> // country id

    @FormUrlEncoded
    @POST("add_specialtrip")
    suspend fun addSpecialTrip(@Header("Authorization") Authorization: String,
                               @Field("from_areaId") from_areaId: Int,
                               @Field("to_areaId") to_areaId: Int,
                               @Field("seat_count") seat_count: Int,
                               @Field("date") date: String,
                               @Field("from_time") from_time: String,
                               @Field("to_time") to_time: String,
                               @Field("price") price: Int,
                               @Field("start_Latitude") start_Latitude: String,
                               @Field("start_Longitude") start_Longitude: String,
                               @Field("end_Latitude") end_Latitude: String,
                               @Field("end_Longitude") end_Longitude: String): Response<BaseResponse>

    @GET("upcomingDriver_Bus")
    suspend fun upcomingBus(@Header("Authorization") Authorization: String): Response<UpcomingBusResponse>

    @GET("completedDriver_Bus")
    suspend fun completedBus(@Header("Authorization") Authorization: String): Response<UpcomingBusResponse>

    @GET("search_specialTrip")
    suspend fun searchSpecialTrip(@Header("Authorization") Authorization: String, @Query("date_from") date_from: String, @Query("date_to") date_to: String): Response<UpcomingBusResponse>

    @GET("show_users_specialTrip")
    suspend fun showUsersSpecialTrip(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int): Response<UsersSuggestedTripResponse>

    @GET("show_user_seats")
    suspend fun showUserSeat(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int, @Query("user_id") user_id: Int): Response<UserSeatsResponse>

    @GET("start_specialTrip")
    suspend fun startSpecialTrip(@Header("Authorization") Authorization: String, @Query("specialTrip_id") specialTrip_id: Int): Response<BaseResponse>

    @GET("end_specialTrip")
    suspend fun endSpecialTrip(@Header("Authorization") Authorization: String, @Query("specialTrip_id") specialTrip_id: Int): Response<BaseResponse>

    @GET("show_user_ByID")
    suspend fun getUserData(@Header("Authorization") Authorization: String, @Query("user_id") user_id: Int): Response<UserDataResponse>

    @Multipart
    @POST("update_profile")
    suspend fun editProfileFunction(@Header("Authorization") Authorization: String,
                                    @Part("first_name") first_name: RequestBody,
                                    @Part("last_name") last_name: RequestBody,
                                    @Part("email") email: RequestBody,
                                    @Part("phone") phone: RequestBody,
                                    @Part part: MultipartBody.Part): Response<BaseResponse>


    @FormUrlEncoded
    @POST("update_profile")
    suspend fun editProfileFunctionWithoutImage(@Header("Authorization") Authorization: String,
                                                @Field("first_name") first_name: String,
                                                @Field("last_name") last_name: String,
                                                @Field("email") email: String,
                                                @Field("phone") phone: String): Response<BaseResponse>

    @GET("show_tripUser_seats")
    suspend fun showSelectedUserSeat(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int, @Query("user_id") user_id: Int): Response<UserBookingDetailsResponse>

    @GET("change_language")
    suspend fun changeLanguage(@Header("Authorization") Authorization: String, @Query("lang") lang: String): Response<BaseResponse>

    @FormUrlEncoded
    @POST("change_passward")
    suspend fun changePassword(@Header("Authorization") Authorization: String, @Field("password") password: String): Response<BaseResponse>

    @GET("my_notification")
    suspend fun getNotification(@Header("Authorization") Authorization: String): Response<NotificationResponse>

    @GET("user_wallet")
    suspend fun userWallet(@Header("Authorization") Authorization: String): Response<WalletResponse>

    @GET("accept_tripSeats")
    suspend fun acceptSpecialTripSeat(@Header("Authorization") Authorization: String, @Query("user_id") user_id: Int, @Query("trip_id") trip_id: Int): Response<BaseResponse>

    @GET("refuse_tripSeats")
    suspend fun refusedSpecialTripSeat(@Header("Authorization") Authorization: String, @Query("user_id") user_id: Int, @Query("trip_id") trip_id: Int): Response<BaseResponse>

    @GET("show_trip_cost")
    suspend fun showTripCost(@Header("Authorization") Authorization: String, @Query("trip_id") trip_id: Int, @Query("time") time: Int, @Query("distance") distance: Int): Response<TripCostResponse>

    //
    @GET("show_area_ByID")
    suspend fun showAreaById(@Header("Authorization") Authorization: String, @Query("city_id") city_id: Int): Response<AreaByIdResponse>


    @GET("show_area_city")
    suspend fun showAreaCity(@Header("Authorization") Authorization: String, @Query("country_id") country_id: Int): Response<ShowAreaCityResponse>

    @GET("forget_password")
    suspend fun forgetPassword(@Query("email") email: String): Response<ForgetPasswordResponse>


    @FormUrlEncoded
    @POST("Reset_password")
    suspend fun resetPassword(@Field("code") code: String, @Field("password") password: String): Response<BaseResponse>

    @FormUrlEncoded
    @POST("area/insert_area")
    suspend fun insertArea(@Header("Authorization") Authorization: String, @Field("name") name: String, @Field("city_id") city_id: Int): Response<BaseResponse>

    @GET("show_driver_trip")
    suspend fun showDriverTrip(@Query("trip_id") trip_id: Int): Response<TripsResponse>

}