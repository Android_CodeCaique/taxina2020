package com.example.taxina2020.ui.activity.auth.phoneOTP

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentPhoneOTPBinding
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

class PhoneOTP : Fragment() {

    lateinit var binding : FragmentPhoneOTPBinding
    lateinit var sharedHelper: SharedHelper
    var countryCodeSelected = "218"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentPhoneOTPBinding.inflate(inflater)
        binding.lifecycleOwner = this
        sharedHelper = SharedHelper()
        binding.btnSendCode.setOnClickListener { checkPhoneValidation(requireContext(), binding.phoneID.text.toString()) }

        binding.ccp.setOnCountryChangeListener {
            //                Toast.makeText(requireContext(),binding.ccp.getSelectedCountryCode(),Toast.LENGTH_LONG).show();
            countryCodeSelected = binding.ccp.selectedCountryCode
        }
        return binding.root
    }

    fun checkPhoneValidation(context: Context, phone: String)
    {
        if (phone.isNotEmpty() ) {

            binding.mkLoaderId.visibility = View.VISIBLE
            val service = ApiClient.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).launch {

                val response = service.checkPhone(phone)
                withContext(Dispatchers.Main) {
                    try {

                        if (response.isSuccessful && response.body()!!.error == 1) {
                            binding.mkLoaderId.visibility = View.INVISIBLE
                            getActivationCode(context,phone)

//                            sharedHelper.putKey(context, "ActiveCode", verificationId)
                            sharedHelper.putKey(context, "VerificationPhone", "+$countryCodeSelected$phone")
                            Navigation.findNavController(requireView()).navigate(R.id.action_phoneOTP_to_verifyPhone)

                        } else if (response.isSuccessful && response.body()!!.error == 0) {
                            Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                            binding.mkLoaderId.visibility = View.INVISIBLE
                        } else {
                            Log.i("ForgetPasswordError", response.message())

                        }
                    } catch (e: Exception) {
                        Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                        binding.mkLoaderId.visibility = View.INVISIBLE

                    }
                }
            }

        } else {
            Toast.makeText(context, "Phone Field Required ", Toast.LENGTH_SHORT).show()
        }
    }

    fun getActivationCode(context: Context, phone: String){

        binding.mkLoaderId.visibility = View.VISIBLE
        Log.d("HEREPHONE", "HEREPHONE : " + phone)
        val activity = context as Activity
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+$countryCodeSelected$phone", // Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                activity, // Activity (for callback binding)
                object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                        binding.mkLoaderId.visibility = View.INVISIBLE

                        Log.d("PHONE", "onVerificationCompleted:$credential")
//                        Toast.makeText(context,"onVerificationCompleted \n $credential",Toast.LENGTH_SHORT).show()
                        //signInWithPhoneAuthCredential(credential)
                    }

                    override fun onVerificationFailed(e: FirebaseException) {
                        Log.w("PHONE", "onVerificationFailed", e)
                        Toast.makeText(context, "onVerificationFailed \n "+resources.getString(R.string.pleaseWriteCorrectFormat), Toast.LENGTH_SHORT).show()
                        binding.mkLoaderId.visibility = View.INVISIBLE

//                        if (e is FirebaseAuthInvalidCredentialsException) {
//
//                        } else if (e is FirebaseTooManyRequestsException) {
//
//                        }
                    }

                    override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                        binding.mkLoaderId.visibility = View.INVISIBLE

//                        Toast.makeText(context, "Code sent", Toast.LENGTH_SHORT).show()
//                        loader.value = false
                        Log.d("PHONE", "onCodeSent:$verificationId")

//                    v?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_registerFragment_to_verifyCodeFragment) }
                    }
                })
    }

}