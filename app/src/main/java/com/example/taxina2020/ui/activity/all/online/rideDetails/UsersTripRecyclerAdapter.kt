package com.example.taxina2020.ui.activity.all.online.rideDetails

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.taxina2020.R
import com.example.taxina2020.databinding.IndividualUserBusDataRowBinding
import com.example.taxina2020.ui.activity.response.UsersSuggestedTripData
import com.example.taxina2020.ui.activity.utils.SharedHelper

class UsersTripRecyclerAdapter (private var dataList: LiveData<List<UsersSuggestedTripData>>,
                                private val context: Context , private val locationFrom:String , private val locationTo:String , private val date:String , private val tripId:Int) : RecyclerView.Adapter<UsersTripRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(IndividualUserBusDataRowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] , locationFrom , locationTo , date , tripId , context)
    }


    class ViewHolder(private var binding: IndividualUserBusDataRowBinding) : RecyclerView.ViewHolder(binding.root) {
        var sharedHelper = SharedHelper()

        fun bind(item: UsersSuggestedTripData , locationFrom: String , locationTo: String , date: String , tripId: Int , context: Context) {
            binding.model = item
            binding.executePendingBindings()
            binding.mainConstraintId.setOnClickListener {
                sharedHelper.putKey(context ,"NotificationIdTrip",tripId.toString())
                sharedHelper.putKey(context ,"NotificationUserID",item.user_id)
                Log.i("UserTripRecyclerTripId",sharedHelper.getKey(context,"NotificationIdTrip"))
                Log.i("UserTripRecyclerUserId",sharedHelper.getKey(context,"NotificationUserID"))
                Navigation.findNavController(it).navigate(R.id.action_rideDetails_to_userBookingDetails)
            }
        }

    }

}