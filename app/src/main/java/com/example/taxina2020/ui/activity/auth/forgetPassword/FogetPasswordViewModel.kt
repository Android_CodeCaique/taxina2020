package com.example.taxina2020.ui.activity.auth.forgetPassword

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.ui.activity.all.Main2Activity
import com.google.firebase.iid.FirebaseInstanceId
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FogetPasswordViewModel : BaseViewModel() {

    var email = MutableLiveData<String>("")
    var loader = MutableLiveData<Boolean>(false)

    fun forgetPassword(v:View , emailET : EditText)
    {
        if(!email.value!!.contains("@") || !email.value!!.contains("."))
        {
            emailET.error = v.rootView.context.resources.getString(R.string.emailFormatIsWrong)
            return
        }
        if(email.value!!.isEmpty())
        {
            emailET.error = v.rootView.context.resources.getString(R.string.emailRequied)
            return
        }
        else {

                loader.value = true
                val service = ApiClient.makeRetrofitServiceHome()
                CoroutineScope(Dispatchers.IO).launch {

                    val response = service.forgetPassword(email.value!!)
                    withContext(Dispatchers.Main) {
                        try {

                            if (response.isSuccessful && response.body()!!.error == 0) {
                                loader.value = false
                                email.value = ""
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                Navigation.findNavController(v).navigate(R.id.action_forgetPasswordFragment_to_resetPasswordFragment)
                            } else if (response.isSuccessful) {
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                loader.value = false
                            } else {
                                Log.i("ForgetPasswordError", response.message())

                            }
                        } catch (e: Exception) {
                            Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                            loader.value = false

                        }
                    }
                }

        }
    }
}