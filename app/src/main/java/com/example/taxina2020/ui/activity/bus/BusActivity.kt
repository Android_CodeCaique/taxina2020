//package com.example.taxina2020.ui.activity.bus
//
//import android.content.Intent
//import android.os.Bundle
//import android.util.Log
//import android.view.MenuItem
//import android.view.View
//import android.widget.ImageButton
//import android.widget.Toast
//import androidx.appcompat.app.AppCompatActivity
//import androidx.constraintlayout.widget.ConstraintLayout
//import androidx.core.view.GravityCompat
//import androidx.drawerlayout.widget.DrawerLayout
//import androidx.fragment.app.FragmentManager
//import androidx.navigation.NavController
//import androidx.navigation.Navigation
//import com.example.taxina2020.R
//import com.example.taxina2020.ui.activity.all.online.home.OnlineAsMicrobus5
//import com.example.taxina2020.ui.activity.all.profile.ProfileAsTaxi7
//import com.example.taxina2020.ui.activity.rides.Rides
//import com.example.taxina2020.ui.activity.rides.UpcomingTap
//import com.google.android.material.navigation.NavigationView
//
//class BusActivity : AppCompatActivity() {
//    val END_SCALE = 0.7f
//    var menuBtn: ImageButton? = null
//    var drawerLayout: DrawerLayout? = null
//    var navigationView: NavigationView? = null
//    var home_content: ConstraintLayout? = null
//    //Fragment fragment2;
//
//    //Fragment fragment2;
//    var manager: FragmentManager? = null
//    var nav: NavController? = null
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_bus)
//
//        manager = supportFragmentManager
//        initiation()
//        navigation()
//        navigationView!!.setNavigationItemSelectedListener()
//        // navigationAnimation();
//        // navigationAnimation();
//        nav = Navigation.findNavController(this, R.id.nav_host_fragment)
//        Log.i("RestartIntent", "1")
//        if (intent.extras != null) {
//            Log.i("RestartIntent", "2")
//            if (intent.getBooleanExtra("openProfile", true)) {
//                if (nav!!.graph.startDestination != nav!!.currentDestination!!.id) {
//                    nav!!.popBackStack(R.id.online5, false)
//                    nav!!.navigate(R.id.action_online5_to_profile7)
//                    Log.i("RestartIntent", "3")
//                } else {
//                    nav!!.navigate(R.id.action_online5_to_profile7)
//                    Log.i("RestartIntent", "4")
//                }
//            }
//        }
//    }
//
//    //    public void goSignup(View view) {
//    //        startActivity(new Intent(getApplicationContext(), Delivery.class));
//    //    }
//    fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
//        return when (menuItem.itemId) {
//            R.id.nav_Home -> {
//                menuBtn!!.visibility = View.VISIBLE
//                drawerLayout!!.closeDrawer(GravityCompat.START)
//                //                OnlineAsTaxi6 onlineAsTaxi6 = new OnlineAsTaxi6();
////                manager.beginTransaction().replace(R.id.relativ1, onlineAsTaxi6).commit();
//                //getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,fragment).commit();
////                Navigation.findNavController()
//                true
//            }
//            R.id.nav_Rides -> {
//                // startActivity(new Intent(getApplicationContext(), Rides.class));
//                menuBtn!!.visibility = View.VISIBLE
//                drawerLayout!!.closeDrawer(GravityCompat.START)
//                startActivity(Intent(this, Rides::class.java))
//                true
//            }
//            R.id.nav_Wallet -> {
//                menuBtn!!.visibility = View.GONE
//                drawerLayout!!.closeDrawer(GravityCompat.START)
//                //                Wallet wallet = new Wallet();
////                manager.beginTransaction().replace(R.id.relativ1, wallet).commit();
//                // getSupportFragmentManager().beginTransaction().replace(R.id.re,wallet).commit();
//                if (nav!!.graph.startDestination != nav!!.currentDestination!!.id) {
//                    nav!!.popBackStack(R.id.online5, false)
//                    nav!!.navigate(R.id.action_online5_to_wallet)
//                } else {
//                    nav!!.navigate(R.id.action_online5_to_wallet)
//                }
//                true
//            }
//            R.id.nav_Notification -> {
//                menuBtn!!.visibility = View.GONE
//                drawerLayout!!.closeDrawer(GravityCompat.START)
//
////                Notification11_1 notification11_1 = new Notification11_1();
////                manager.beginTransaction().replace(R.id.relativ1, notification11_1).commit();
//                //getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,fragment2).commit();
//                if (nav!!.graph.startDestination != nav!!.currentDestination!!.id) {
//                    nav!!.popBackStack(R.id.online5, false)
//                    nav!!.navigate(R.id.action_online5_to_notification11_1)
//                } else {
//                    nav!!.navigate(R.id.action_online5_to_notification11_1)
//                }
//                true
//            }
//            R.id.nav_Settings -> {
//                Toast.makeText(applicationContext, "" + menuItem.title, Toast.LENGTH_LONG).show()
//                true
//            }
//            else -> false
//        }
//    }
//
//    private fun initiation() {
//        drawerLayout = findViewById(R.id.drawer_layout)
//        navigationView = findViewById(R.id.navigation_view)
//        menuBtn = findViewById(R.id.menu)
//        home_content = findViewById(R.id.home_content)
//    }
//
//    fun navigation() {
//        navigationView!!.bringToFront()
//        navigationView!!.setCheckedItem(R.id.nav_Home)
//        val onlineAsMicrobus5 = OnlineAsMicrobus5()
//        //        manager.beginTransaction().replace(R.id.relativ1, onlineAsMicrobus5).commit();
//        if (UpcomingTap.DontShow == true) {
//            val profileAsTaxi7 = ProfileAsTaxi7()
//            //            manager.beginTransaction().replace(R.id.relativ1, profileAsTaxi7).commit();
//        }
//        // getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,fragment2).commit();
//        menuBtn!!.setOnClickListener {
//            if (drawerLayout!!.isDrawerVisible(GravityCompat.START)) {
//                drawerLayout!!.closeDrawer(GravityCompat.START)
//            } else drawerLayout!!.openDrawer(GravityCompat.START)
//        }
//    }
//
//}
