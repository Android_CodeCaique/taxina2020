package com.example.taxina2020.ui.activity.auth.addCar

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.taxina2020.R
import com.example.taxina2020.ui.activity.all.Main2Activity
import com.example.taxina2020.ui.activity.response.AddCarResponse
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import retrofit2.Response

class AddCarViewModel : BaseViewModel() {

    var loader = MutableLiveData<Boolean>(false)
    var sharedHelper: SharedHelper = SharedHelper()
    var carTypeListRespoName = ArrayList<String>()
    var carTypeListRespoId = ArrayList<Int>()


    var carType = MutableLiveData<Int>(0)
    var modelName = MutableLiveData<String>("")
    var year = MutableLiveData<String>("")
    var plate = MutableLiveData<String>("")
    var color = MutableLiveData<String>("")
    var bookingType = MutableLiveData<String>("")
    var drivingLicneceNumber = MutableLiveData<String>("")
    var carCatId = MutableLiveData<Int>(0)
    var carCatIdSelected = MutableLiveData<Boolean>(false)
    var carName = MutableLiveData<String>("")



    fun getSuggestedCar(context: Context)
    {
        Log.i("carType",carType.value.toString())
        Log.i("TokenLog",sharedHelper.getKey(context, "Token"))
        loader.value = true
        val service = ApiClient.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).async {

            val response = service.showSuggestedCar("Bearer ${sharedHelper.getKey(context, "TokenRegister")}",carType.value!!)

            withContext(Dispatchers.Main) {
                try {

                    if (response.isSuccessful && response.body()!!.error == 0) {
                        for (i in response.body()!!.data) {
                            carTypeListRespoName.add(i.name)
                            carTypeListRespoId.add(i.id)
                        }

                        Log.i("Response",response.body()!!.data.toString())
                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false

                    } else {

                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        loader.value = false
                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
                    loader.value = false

                }
            }
        }
    }

    fun addCar(v:View , yearET : EditText ,plateET : EditText ,colorET:EditText , drivingLicenseET:EditText ,modelET : EditText)
    {

        Log.i("Car Name VM",carName.value)
        Log.i("Car ID VM",carCatId.value.toString())
//        if( carCatIdSelected.value == false || carName.value?.isEmpty()!! || modelName.value?.isEmpty()!! || plate.value?.isEmpty()!! || color.value?.isEmpty()!! || year.value?.isEmpty()!! || drivingLicneceNumber.value?.isEmpty()!!)
//        {
//            Toast.makeText(v.rootView.context,"All field required !",Toast.LENGTH_SHORT).show()
//        }
        if(carName.value?.isEmpty()!!) {
            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.selectCarFromCategore),Toast.LENGTH_SHORT).show()
        }
        if(modelName.value?.isEmpty()!!){
            modelET.error = v.rootView.context.resources.getString(R.string.modelRequied)
        }
        if(year.value?.isEmpty()!!){
            yearET.error = v.rootView.context.resources.getString(R.string.yearRequied)
        }
        if(plate.value?.isEmpty()!!){
            plateET.error = v.rootView.context.resources.getString(R.string.plateRequied)
        }
        if(color.value?.isEmpty()!!){
            colorET.error = v.rootView.context.resources.getString(R.string.colorRequied)
        }
        if(drivingLicneceNumber.value?.isEmpty()!!){
            drivingLicenseET.error = v.rootView.context.resources.getString(R.string.drivingLicenseRequied)
        }
        else {
            loader.value = true
            val service = ApiClient.makeRetrofitService()
            CoroutineScope(Dispatchers.IO).async {

                val response = service.addCar("Bearer ${sharedHelper.getKey(v.rootView.context, "TokenRegister")}", carType.value!!, carCatId.value!!, carName.value!!, modelName.value!!, plate.value!!, color.value!!, year.value!!,drivingLicneceNumber.value!!)

                withContext(Dispatchers.Main) {
                    try {

                        if (response.isSuccessful && response.body()!!.error == 0) {
//                            val alert = AlertDialog.Builder(v.rootView.context)
//                            alert.setView(R.layout.add_car_success_dialog)
//                            alert.create().show()

//                            showAlert(v.rootView.context)
                            loader.value = false

                            showAlert(v.rootView.context,response)
                        } else {

                            Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                            loader.value = false
                        }
                    } catch (e: Exception) {
                        Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                        loader.value = false

                    }
                }
            }
        }
    }


    fun showAlert(context: Context ,response: Response<AddCarResponse>)
    {
        val alert: Dialog? = Dialog(context)
        alert?.setContentView(R.layout.add_car_success_dialog)
//        alert?.setCanceledOnTouchOutside(false)
        alert?.setCancelable(false)
        alert?.findViewById<Button>(R.id.cancelBtnId)?.setOnClickListener {
            try {
                sharedHelper.putKey(context, "Token", sharedHelper.getKey(context, "TokenRegister"))
                sharedHelper.putKey(context, "fName", response.body()!!.data.first_name)
                sharedHelper.putKey(context, "lName", response.body()!!.data.last_name)
                sharedHelper.putKey(context, "Image", response.body()!!.data.image)
                sharedHelper.putKey(context, "E-Mail", response.body()!!.data.email)
                sharedHelper.putKey(context, "Phone", response.body()!!.data.phone)
                sharedHelper.putKey(context, "UserId", response.body()!!.data.id.toString())
                sharedHelper.putKey(context, "UserIsOnline", response.body()!!.data.is_online)
                sharedHelper.putKey(context, "OPEN", "OPEN")

                if (carType.value == 1) {
                    sharedHelper.putKey(context, "car_type", "1") // car
                } else if (carType.value == 2) {
                    sharedHelper.putKey(context, "car_type", "2") // bus
                }

                val intent = Intent(context, Main2Activity::class.java)
                context.startActivity(intent)
                (context as Activity).finish()

                alert.dismiss()
            }catch (e:Exception)
            {

            }
        }

        alert?.show()
    }
}