package com.example.taxina2020.ui.activity.networking

import com.example.taxina2020.ui.activity.response.AllCountryResponse
import com.example.taxina2020.ui.activity.response.ShowAreaCityResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface CountryUser {

    @GET("country/show_country")
    suspend fun showCountries() : Response<AllCountryResponse>


}