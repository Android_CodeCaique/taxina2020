package com.example.taxina2020.ui.activity.auth.login

import ApiClient
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.taxina2020.R
import com.example.taxina2020.ui.activity.all.Main2Activity
//import com.example.taxina2020.ui.activity.bus.BusActivity
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.google.firebase.iid.FirebaseInstanceId

class LoginViewModel : BaseViewModel() {

    var email = MutableLiveData<String>("")
    var password = MutableLiveData<String>("")
    var loader = MutableLiveData<Boolean>(false)
    var sharedHelper:SharedHelper = SharedHelper()
    var locationManager: LocationManager? = null
    var GpsStatus = false

    fun login(v: View , emailET:EditText , passwordET:EditText)
    {
        if (email.value!!.isNotEmpty() &&  password.value!!.isNotEmpty()) {

            if (CheckGpsStatus(v)) {
                loader.value = true
                val token :String? = FirebaseInstanceId.getInstance().token
                val service = ApiClient.makeRetrofitService()
                Log.i("Login", "1")
                CoroutineScope(Dispatchers.IO).launch {
                    Log.i("Login", "2")

                    val response = service.login(email.value!!, password.value!!,token+"")
                    Log.i("Response1", response.message())
                    Log.i("Response2", response.code().toString())
                    withContext(Dispatchers.Main) {
                        try {
                            Log.i("Register", "3")

                            if (response.isSuccessful && response.body()!!.error == 0) {
                                Log.i("Login", "4")
                                Log.i("Login", response.message())
                                loader.value = false
                                email.value = ""
                                password.value = ""
                                sharedHelper.putKey(v.context, "Token", response.body()!!.data.token)
                                sharedHelper.putKey(v.context, "fName", response.body()!!.data.first_name)
                                sharedHelper.putKey(v.context, "lName", response.body()!!.data.last_name)
                                sharedHelper.putKey(v.context, "Image", response.body()!!.data.image)
                                sharedHelper.putKey(v.context, "E-Mail", response.body()!!.data.email)
                                sharedHelper.putKey(v.context, "Phone", response.body()!!.data.phone)
                                sharedHelper.putKey(v.context, "UserId", response.body()!!.data.id.toString())
                                sharedHelper.putKey(v.context, "LoginUserCountryId", response.body()!!.data.country_id)
                                sharedHelper.putKey(v.context, "UserIsOnline", response.body()!!.data.is_online)
                                sharedHelper.putKey(v.rootView.context, "OPEN", "OPEN")

//                                sharedHelper.putKey(v.context, "DriverFirstName", response.body()!!.data.first_name)
//                                sharedHelper.putKey(v.context, "DriverImage", response.body()!!.data.image.toString())
                                if(response.body()!!.data.car_type == "1") {
                                    sharedHelper.putKey(v.context, "car_type","1") // car
                                }else if(response.body()!!.data.car_type == "2") {
                                    sharedHelper.putKey(v.context, "car_type","2") // bus
                                }

                                val intent = Intent(v.context, Main2Activity::class.java)
                                v.context.startActivity(intent)
                                (v.context as Activity).finish()
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()

                            } else if (response.isSuccessful) {
                                Toast.makeText(v.rootView.context, response.body()!!.message, Toast.LENGTH_SHORT).show()
                                loader.value = false
                            } else {
                                Log.i("RegisterError", response.message())

                            }
                        } catch (e: Exception) {
                            Toast.makeText(v.rootView.context, e.message.toString(), Toast.LENGTH_SHORT).show()
                            loader.value = false

                        }
                    }
                }
            }else{
                Toast.makeText(v.rootView.context, v.rootView.context.resources.getString(R.string.youNeedToOpenGPS), Toast.LENGTH_SHORT).show()

            }
        } else {
//            Toast.makeText(v.rootView.context, "All Field Required ", Toast.LENGTH_SHORT).show()
            if(email.value!!.isEmpty())
            {
//                Toast.makeText(v.rootView.context, "Email Field Required ", Toast.LENGTH_SHORT).show()
                emailET.error = v.rootView.context.resources.getString(R.string.emailRequied)
            }
            if(password.value!!.isEmpty())
            {
//                Toast.makeText(v.rootView.context, "Password Field Required ", Toast.LENGTH_SHORT).show()
                passwordET.error = v.rootView.context.resources.getString(R.string.passwordRequied)

            }
        }
    }

    private fun CheckGpsStatus(v:View) : Boolean {
        locationManager = v.rootView.context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        assert(locationManager != null)
        GpsStatus = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return GpsStatus
    }
}