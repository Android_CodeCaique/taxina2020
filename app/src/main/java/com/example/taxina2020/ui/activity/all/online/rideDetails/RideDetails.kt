package com.example.taxina2020.ui.activity.all.online.rideDetails

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.databinding.FragmentRideDetailsBinding
import com.example.taxina2020.ui.activity.response.UsersSuggestedTripData
import com.example.taxina2020.ui.activity.utils.SharedHelper
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.FirebaseDatabase
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class RideDetails : Fragment() {
    //    ConstraintLayout conOffline, cinAsMicrobus5;
    //    TextView tv_offline, tv_online;
    //    ImageView openTop, openChat;
    //    @NonNull
    //    @Override
    //    protected Navigator<? extends FragmentNavigator.Destination> createFragmentNavigator() {
    //        return super.createFragmentNavigator();
    //
    //    }
    var mLocationManager: LocationManager? = null

    lateinit var binding : FragmentRideDetailsBinding
    var rideDetailsViewModel: RideDetailsViewModel? = null
    var users: MutableLiveData<List<UsersSuggestedTripData>> = MutableLiveData()
    lateinit var sharedHelper : SharedHelper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRideDetailsBinding.inflate(inflater)
        binding.lifecycleOwner = this
        sharedHelper = SharedHelper()

        rideDetailsViewModel = ViewModelProviders.of(this).get(RideDetailsViewModel::class.java)
        binding.vm = rideDetailsViewModel
//        val args = arguments?.let { RideDetailsArgs.fromBundle(it) }

        Log.i("BusTripId1",sharedHelper.getKey(requireContext(),"BusTripId"))

        rideDetailsViewModel?.locationFrom?.value = sharedHelper.getKey(requireContext(),"BusTripLocationFrom")
        rideDetailsViewModel?.locationTo?.value = sharedHelper.getKey(requireContext(),"BusTripLocationTo")
        rideDetailsViewModel?.date?.value = sharedHelper.getKey(requireContext(),"BusTripDate")
        rideDetailsViewModel?.tripId?.value = sharedHelper.getKey(requireContext(),"BusTripId")?.toInt()

        rideDetailsViewModel?.getTripUsers(requireContext())
        rideDetailsViewModel?.users?.observe(viewLifecycleOwner, Observer {
            users.value = it
            binding.usersRecyclerViewId.adapter = UsersTripRecyclerAdapter(users,requireContext()
                    , sharedHelper.getKey(requireContext(),"BusTripLocationFrom").toString(),
                    sharedHelper.getKey(requireContext(),"BusTripLocationTo").toString(),
                    sharedHelper.getKey(requireContext(),"BusTripDate").toString() ,
                    sharedHelper.getKey(requireContext(),"BusTripId")?.toInt()!!)
        })
        Log.i("BusTripId2",sharedHelper.getKey(requireContext(),"BusTripId"))


//        sharedHelper.putKey(requireContext(),"BusTripId","")
//        sharedHelper.putKey(requireContext(),"BusTripLocationFrom","")
//        sharedHelper.putKey(requireContext(),"BusTripLocationTo","")
//        sharedHelper.putKey(requireContext(),"BusTripDate","")
        Log.i("BusTripId3",sharedHelper.getKey(requireContext(),"BusTripId"))


//        mapView = (MapView) view.findViewById(R.id.mapViewId);

        mLocationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0f, mLocationListener)
        }


        return binding.root
    }


    private fun sendLocationToFirebase(userId: String, lat: Double, lon: Double) {
        Log.i("AllowFirebaseUser", userId)
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("driverLocation")
        val map: MutableMap<String, String> = HashMap()
        map["lat"] = lat.toString()
        map["lon"] = lon.toString()
        map["userId"] = userId
        myRef.child(userId).setValue(map)
        Log.i("AllowFirebaseSta", "true")
    }

    private val mLocationListener: LocationListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            try {
                sendLocationToFirebase(sharedHelper.getKey(requireContext(), "UserId").toString(), location.latitude, location.longitude)
            }catch (e:Exception){
                Toast.makeText(requireContext(),e.message.toString(),Toast.LENGTH_SHORT).show()
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

}
