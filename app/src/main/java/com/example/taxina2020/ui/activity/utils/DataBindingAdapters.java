package com.example.taxina2020.ui.activity.utils;

import android.view.View;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.example.taxina2020.R;

public class DataBindingAdapters implements androidx.databinding.DataBindingComponent {

    @BindingAdapter( "imageUrl" )
    public static void loadImage(ImageView imageView, String imageURL)
    {
        Glide.with(imageView.getContext())
                .load(imageURL)
                .into(imageView);
    }

//    @BindingAdapter("seatType")
//    public static void loadSeatImage(View view , int num)
//    {
//        view.setBackgroundResource(view.getContext().getResources().getDrawable(R.drawable.sea));
//    }

}
