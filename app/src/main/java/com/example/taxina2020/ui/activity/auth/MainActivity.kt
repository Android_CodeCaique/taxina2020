package com.example.taxina2020.ui.activity.auth

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.ui.activity.all.Main2Activity
import com.example.taxina2020.ui.activity.utils.SharedHelper
import java.security.MessageDigest
import java.util.*

class MainActivity : AppCompatActivity() {
    var sharedHelper: SharedHelper? = null
    var nav: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        printHashKey(this)
        // navigationAnimation();
        nav = Navigation.findNavController(this, R.id.fragmentIntro)

        sharedHelper = SharedHelper()
        setLocate(sharedHelper?.getKey(this, "MyLang").toString())
        val Token = sharedHelper!!.getKey(this, "Token")
        if (!Token!!.isEmpty()) {
            startActivity(Intent(this, Main2Activity::class.java))
            finish()
        }
        val SharedHelpers : SharedHelper =  SharedHelper()
        SharedHelpers.putKey(this, "OPEN", "OPEN")
    }

    override fun onBackPressed() {
        if(nav!!.currentDestination!!.label == "fragment_add_car" || nav!!.currentDestination!!.label == "fragment_sign_up")
        {
//            Toast.makeText(this,"hhhh",Toast.LENGTH_SHORT).show()
        }
        else
           super.onBackPressed()
    }

    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
        sharedHelper?.putKey(this, "MyLang", Lang)
    }

    fun printHashKey(context: Context) {
        try {
            val info = context.packageManager
                    .getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i("AppLog11", "key:$hashKey=")
            }
        } catch (e: java.lang.Exception) {
            Log.e("AppLog22", "error:", e)
        }
    }

}