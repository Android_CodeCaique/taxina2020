package com.example.taxina2020.ui.activity.response

data class AllCityResponse(
    val data: List<AllCityData>,
    val error: Int,
    val message: String
)

data class AllCityData(
    val created_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)