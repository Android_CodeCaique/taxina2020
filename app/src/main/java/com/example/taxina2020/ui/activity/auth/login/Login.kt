package com.example.taxina2020.ui.activity.auth.login

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentLoginBinding
import com.example.taxina2020.ui.activity.all.Main2Activity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.shoohna.shoohna.util.base.BaseFragment
import org.json.JSONException
import org.json.JSONObject
import java.net.MalformedURLException
import java.net.URL

/**
 * A simple [Fragment] subclass.
 */
class Login : Fragment() {
//    var mGoogleSignInClient: GoogleSignInClient? = null
    var binding: FragmentLoginBinding? = null
    var loginViewModel: LoginViewModel? = null
    var callbackManager : CallbackManager?= null
    lateinit var baseFragment: BaseFragment
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater)
        binding!!.lifecycleOwner = this
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding!!.vm = loginViewModel
        baseFragment = BaseFragment()
//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build()
//        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)
        binding!!.google.setOnClickListener {
           clickOnGoogle()
        }

        binding!!.face.setOnClickListener {
            clickOnFaceBook()
        }
        return binding!!.root
    }


    private fun clickOnFaceBook ()
    {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
                .logInWithReadPermissions(this, listOf("email", "public_profile"))
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult?) {
                        //To change body of created functions use File | Settings | File Templates.
                        // Here we will add to get photo , email
                        // App code
                        val UserId: String = loginResult!!.accessToken.userId

                        val graphRequest = GraphRequest.newMeRequest(
                                loginResult!!.accessToken
                        ) { `object`, _ -> getData(`object`) }
                        val parameters = Bundle()
                        parameters.putString("fields", "first_name , last_name , email , id")
                        graphRequest.parameters = parameters
                        graphRequest.executeAsync()
                    }

                    override fun onError(error: FacebookException?) {
                        //To change body of created functions use File | Settings | File Templates.
                        Log.d("Here", "Here 1")
                    }

                    override fun onCancel() {
                        //To change body of created functions use File | Settings | File Templates.
                        Log.d("Here", "Here 2")
                    }
                })
    }

    private fun getData(`object`: JSONObject) {
        var first_name: String?
        var last_name: String?
        var email: String?
        val id: String
        first_name = ""
        last_name = ""
        email = ""
        val url: URL
        try {
            first_name = `object`.getString("first_name")
            last_name = `object`.getString("last_name")
            email = `object`.getString("email")
            id = `object`.getString("id")
            url =
                    URL("https://graph.facebook.com/" + `object`.getString("id") + "/picture?width=250&height=250")
            // LoginFBFunction(first_name+" "+last_name , email , id , url.toString());
//            loginViewModel.login(requireView(),email)
//            loginViewModel.login(requireContext(),email)

            Toast.makeText(requireContext(),email,Toast.LENGTH_SHORT).show()
            Log.d("IMAGE", url.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(requireContext(),e.message,Toast.LENGTH_SHORT).show()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            Toast.makeText(requireContext(),e.message,Toast.LENGTH_SHORT).show()

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = Navigation.findNavController(view)
        binding!!.tvSignup.setOnClickListener { navController.navigate(R.id.action_login_to_phoneOTP) }
        binding!!.tvForget.setOnClickListener { navController.navigate(R.id.action_login_to_forgetPasswordFragment) }

//        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(ContextCompat.checkSelfPermission(
//                        getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
//                        PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
//                        getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) ==
//                        PackageManager.PERMISSION_GRANTED){
//                    Intent intent =new Intent(getContext(), Main2Activity.class);
//                    startActivity(intent);
//                }
//                else
//                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
//            }
//        });
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            val intent = Intent(context, Main2Activity::class.java)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        else
        {
            callbackManager?.onActivityResult(requestCode , resultCode , data)

        }
    }

    private fun  handleSignInResult( completedTask : Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val email  : String? = account!!.email
//            loginViewModel.login(requireContext(),email)
        } catch ( e :Exception) {

        }
    }

    private fun clickOnGoogle()
    {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        val  mGoogleSignInClient = GoogleSignIn.getClient(requireView().context, gso)

        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, 1)

        val account = GoogleSignIn.getLastSignedInAccount(requireView().context)
    }
}