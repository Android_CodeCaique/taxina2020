package com.example.taxina2020.ui.activity.roomDB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class StepsModel (@PrimaryKey val user_id: String,
                       @ColumnInfo(name = "trip_id") val trip_id: String,
                       @ColumnInfo(name = "steps") val steps: Double,
                       @ColumnInfo(name = "meters")val meters: Double)