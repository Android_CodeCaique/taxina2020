package com.example.taxina2020.ui.activity.response

data class ShowAreaCityResponse(
    val data: List<ShowAreaCityData>,
    val error: Int,
    val message: String
)

data class ShowAreaCityData(
    val area_id: String,
    val area_name: String,
    val city_name: String
)