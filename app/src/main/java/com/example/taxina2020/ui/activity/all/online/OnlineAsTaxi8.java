package com.example.taxina2020.ui.activity.all.online;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.taxina2020.R;

import static com.example.taxina2020.ui.activity.all.chat.Chat.currantFrag;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnlineAsTaxi8 extends Fragment {

    ConstraintLayout conOffline, conOnline;
    ImageView message;
    Button btn_iAmHere;
    TextView tv_offline, tv_online;

    ImageButton menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online_as_taxi8, container, false);

        initiation(view);


        tv_offline = view.findViewById(R.id.tv_offline);
        tv_offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conOffline.setVisibility(View.GONE);
                conOnline.setVisibility(View.VISIBLE);

            }
        });

        tv_online = view.findViewById(R.id.tv_online);
        tv_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conOnline.setVisibility(View.GONE);
                conOffline.setVisibility(View.VISIBLE);

            }
        });

        if (conOnline.getVisibility() == View.VISIBLE) {

            message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Chat chat = new Chat();
//                    FragmentManager manager = getFragmentManager();
//                    manager.beginTransaction().replace(R.id.relativ1, chat).commit();
//                    Navigation.findNavController(v).navigate(R.id.action_onlineAsTaxi8_to_chat);
                }
            });

            btn_iAmHere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    OnlineAsTaxi9 onlineAsTaxi9 = new OnlineAsTaxi9();
//                    FragmentManager manager = getFragmentManager();
//                    manager.beginTransaction().replace(R.id.relativ1, onlineAsTaxi9).commit();
//                    Navigation.findNavController(v).navigate(R.id.action_onlineAsTaxi8_to_onlineAsTaxi9);
                }
            });
        }

        return view;
    }

    private void initiation(View view) {
        menu = getActivity().findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);

        conOffline = view.findViewById(R.id.conOffline);
        conOnline = view.findViewById(R.id.conOnline);
        message = view.findViewById(R.id.message);
        btn_iAmHere = view.findViewById(R.id.btn_iAmHere);

        currantFrag = R.layout.fragment_online_as_taxi6;


    }
}
