package com.example.taxina2020.ui.activity.rides

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentUpcomingTapBinding
import com.example.taxina2020.ui.activity.all.online.busHome.BusTripsHomeRecyclerAdapter
import com.example.taxina2020.ui.activity.response.CompletedTripData
import com.example.taxina2020.ui.activity.response.UpcomingBusData
import com.example.taxina2020.ui.activity.utils.SharedHelper

/**
 * A simple [Fragment] subclass.
 */
class UpcomingTap : Fragment() {
    var conShowProfile: ConstraintLayout? = null
    private var sharedHelper: SharedHelper ? = null

    lateinit var binding: FragmentUpcomingTapBinding
    var trips: MutableLiveData<List<CompletedTripData>> = MutableLiveData()
    var usersBusUpcoming: MutableLiveData<List<UpcomingBusData>> = MutableLiveData()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment


        binding = FragmentUpcomingTapBinding.inflate(inflater)
        binding.lifecycleOwner = this
        sharedHelper = SharedHelper()
        val ridesViewModel = ViewModelProviders.of(this).get(RidesViewModel::class.java)
        binding.vm = ridesViewModel
        Log.i("Rides","out")
        Log.i("Rides",sharedHelper?.getKey(requireContext(),"car_type").toString())

        if (sharedHelper?.getKey(requireActivity(), "car_type") == "1") {
            Log.i("Rides","1")
            ridesViewModel.getSpecialTrips(requireContext())
            ridesViewModel.speacialTrips.observe(viewLifecycleOwner, Observer {
                if (it.isNotEmpty()) {
                    trips.value = it
                    binding.tripsRecyclerId.adapter = SpecialTripRecyclerAdapter(trips, activity,ridesViewModel)
                }
            })
        }

        if (sharedHelper?.getKey(requireActivity(), "car_type") == "2") {
            Log.i("Rides","2")

            ridesViewModel.getBusSpecialTrips(requireContext())
            ridesViewModel.usersBusUpcoming.observe(viewLifecycleOwner, Observer {
                usersBusUpcoming.value = it
                binding.tripsRecyclerId.adapter =  CompletedTripBusRecyclerAdapter(usersBusUpcoming,requireContext())

            })

        }


        return binding.root

//        conShowProfile = view.findViewById(R.id.conShowProfile)
//        conShowProfile.setOnClickListener(View.OnClickListener {
//            val intent = Intent(context, Main2Activity::class.java)
//            intent.putExtra("openProfile", true)
//            startActivity(intent)
//            DontShow = true
//            //                ProfileAsTaxi7 profileAsTaxi7=new ProfileAsTaxi7();
////                FragmentManager manager=getFragmentManager();
////                DontShow=true;
////                manager.beginTransaction().replace(R.id.main_fragment2,profileAsTaxi7,profileAsTaxi7.getTag()).commit();
//        })

    }

    companion object {
        @JvmField
        var DontShow = false
    }
}