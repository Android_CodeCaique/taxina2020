package com.example.taxina2020.ui.activity.auth.forgetPassword

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentForgetPasswordBinding

class ForgetPasswordFragment : Fragment() {

    lateinit var binding : FragmentForgetPasswordBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = FragmentForgetPasswordBinding.inflate(inflater)
        binding.lifecycleOwner = this
        val vm = ViewModelProviders.of(this).get(FogetPasswordViewModel::class.java)
        binding.vm = vm



        return binding.root
    }

}