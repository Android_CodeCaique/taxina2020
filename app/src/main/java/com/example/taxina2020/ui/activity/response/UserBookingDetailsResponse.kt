package com.example.taxina2020.ui.activity.response

data class UserBookingDetailsResponse(
    val data : List<UserBookingDetailsData>,
    val details: UserBookingDetailsDetails,
    val error: Int,
    val message: String
)

data class UserBookingDetailsData(
    val id: Int,
    val is_ride: String,
    val money: String,
    val seat_num: String,
    val user_id: String
)

data class UserBookingDetailsDetails(
    val date: String,
    val first_name: String,
    val from_area: String,
    val from_areaId: String,
    val image: String,
    val is_ride: String,
    val last_name: String,
    val price: String,
    val to_area: String,
    val to_areaId: String,
    val user_id: String
)