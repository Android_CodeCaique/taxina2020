package com.example.taxina2020.ui.activity.rides

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.taxina2020.R
import com.example.taxina2020.databinding.FragmentCurrentTapBinding
import com.example.taxina2020.ui.activity.response.TripsResponse
import com.example.taxina2020.ui.activity.utils.SharedHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CurrentTabViewModel : ViewModel() {

    var loader = MutableLiveData<Boolean>(true)
    var btnEnable = MutableLiveData<Boolean>(true)
    var cardVisible = MutableLiveData<Boolean>(false)
    var mBinding = MutableLiveData<FragmentCurrentTapBinding>()
    var sharedHelper: SharedHelper = SharedHelper()
    var currentTripLiveData = MutableLiveData<TripsResponse>()

    companion object {
        private val TAG = CurrentTabViewModel::class.simpleName
    }

    suspend fun getCurrentTrip(mContext: Context) {

        val services = ApiClient.makeRetrofitServiceHome()
        val tripId = try {
            sharedHelper.getKey(mContext, "ChatTripId")!!.toInt()

        } catch (exception: Exception) {
            Toast.makeText(mContext, mContext.getString(R.string.no_current_trips_founded), Toast.LENGTH_SHORT).show()
            loader.value = false
            return
        }


        withContext(CoroutineScope(Dispatchers.IO).coroutineContext) {

            kotlin.runCatching { services.showDriverTrip(tripId) }
                    .onSuccess {
                        loader.value = false
                        cardVisible.value = true
                        currentTripLiveData.value = it.body()!!

                    }
                    .onFailure {
                        loader.value = false
                        Log.e(TAG, "getCurrentTrip: ${it.message}")
                        Toast.makeText(mContext, mContext.getString(R.string.some_thing_error), Toast.LENGTH_SHORT).show()
                    }

        }

    }

    fun requestCancelTrip() {

         val mContext=mBinding.value!!.root.context

        val tripId =
                sharedHelper.getKey(mContext, "ChatTripId")!!

        val service = ApiClient.makeRetrofitServiceHome()
        loader.value=true
        btnEnable.value=false

        viewModelScope.launch(Dispatchers.IO) {

            kotlin.runCatching { service.driverRequestCancelTrip(tripId) }
                    .onSuccess {
                        loader.value=false

                    }
                    .onFailure {
                        btnEnable.value=true
                        loader.value = false
                        Log.e(TAG, "getCurrentTrip: ${it.message}")
                        Toast.makeText(mContext,mContext.getString(R.string.some_thing_error) , Toast.LENGTH_SHORT).show()

                    }

        }

    }

}